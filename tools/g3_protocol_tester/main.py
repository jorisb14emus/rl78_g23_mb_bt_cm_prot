
from g3_protocol_tester.tester import process_user_input

from l3_codec.l3_encoder import L3Encoder
from l3_codec.l3_packet import L3Packet
from serial_usb_transceiver.serial_usb_transceiver import SerialUsbTransceiver


def main() -> None:
	SerialUsbTransceiver.bind('COM4', 112500, timeout=1)

	while True:
		l3_packet: L3Packet | None = process_user_input()
		if l3_packet is None:
			continue

		SerialUsbTransceiver.transmit_l3_packet(l3_packet)
		while SerialUsbTransceiver._serial_port.in_waiting > 0:
			l3_packet: L3Packet = SerialUsbTransceiver.receive_l3_packet()
			if l3_packet is not None:
				raw_l3_packet: bytearray = L3Encoder.encode(l3_packet)
				if raw_l3_packet is not None:
					L3Encoder.print(raw_l3_packet)


if __name__ == '__main__':
	main()
