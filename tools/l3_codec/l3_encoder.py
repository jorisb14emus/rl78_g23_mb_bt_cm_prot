
from l3_codec.l3_packet import L3Packet, L3TransferType, L3PacketType


class L3Encoder:
	@staticmethod
	def encode(l3_packet: L3Packet) -> bytearray | None:
		assert l3_packet is not None, "[fatal]: provided l3_packet was None!"

		transfer_type: int = l3_packet.header.transfer_type & 0b00000011
		packet_type: int = l3_packet.header.packet_type & 0b00001111
		direction: int = l3_packet.header.direction & 0b00000001
		status: int = l3_packet.header.status & 0b00000001
		body_length: int = l3_packet.header.body_length

		if transfer_type == L3TransferType.BROADCAST:
			body_data: bytearray = l3_packet.inner.body.data
		elif transfer_type == L3TransferType.UNICAST:
			body_data: bytearray = bytearray([
				l3_packet.inner.header.recipient
			]) + l3_packet.inner.body.data
		elif transfer_type == L3TransferType.MULTICAST:
			body_data: bytearray = bytearray([
				l3_packet.inner.header.bottom_recipient,
				l3_packet.inner.header.top_recipient
			]) + l3_packet.inner.body.data
		else:
			assert False, "fatal: should never reach this block!"

		first_l3_header_byte: int = \
			(transfer_type << 0) + \
			(packet_type   << 2) + \
			(direction     << 6) + \
			(status        << 7)

		second_l3_header_byte: int = body_length
		payload: bytearray = bytearray([
			first_l3_header_byte, second_l3_header_byte,  # L3 header bytes
		]) + body_data

		return payload

	@staticmethod
	def print(payload: bytearray, packet_type: int | None = None, end_line: str = '\n') -> None:
		assert payload is not None, "[fatal]: provided payload was None!"

		def bit_mask(offset: int, bit_count: int) -> int:
			return ((0x01 << bit_count) - 1) << offset

		def read_value(payload: bytearray, bits_in_value: int, bits_offset: int) -> int:
			byte_offset: int = bits_offset // 8
			bit_offset: int = bits_offset % 8
			current_value: int = int.from_bytes(payload[byte_offset:byte_offset + 4], byteorder='little')
			shifted_right_value: int = current_value >> bit_offset
			masked_value: int = shifted_right_value & bit_mask(0, bits_in_value)
			return masked_value

		if packet_type is None:
			count: int = len(payload) - 2
			bits_steps: list[int] = [8] * count
		else:
			bits_steps: list[int] = L3PacketType.get_bits_steps(packet_type)
			if bits_steps is None:
				count: int = len(payload) - 2
				bits_steps: list[int] = [8] * count

		temp_list: list[int] = [2, 4, 1, 1, 8]
		temp_list.extend(bits_steps)
		bits_steps = temp_list

		print(f'length:  ', end=' ')
		for step in bits_steps:
			string: str = f'Bits ({step})'
			string = string.ljust(step if step > 8 else 8)
			print(f'{string}', end=' ')
		print()

		print(f'binary:  ', end=' ')
		bits_offset: int = 0
		for index, step in enumerate(bits_steps):
			value: int = read_value(payload, step, bits_offset)
			bits_offset += step
			string: str = f'{value:0{step}b}'
			string = string.ljust(8)
			print(f'{string}', end=' ')
		print()

		print(f'decimal: ', end=' ')
		bits_offset: int = 0
		for index, step in enumerate(bits_steps):
			value: int = read_value(payload, step, bits_offset)
			bits_offset += step
			string: str = f'{value}'.ljust(step if step > 8 else 8)
			print(f'{string}', end=' ')
		print(f'{end_line}')
