
from l3_codec.l3_packet import L3Packet, L3TransferType, L3PacketDirection, L3PacketStatus, L3PacketType


class L3Decoder:
	@staticmethod
	def decode(payload: bytearray) -> L3Packet | None:
		assert payload is not None, "[fatal]: provided payload was None!"

		# NOTE: NOT skipping any bytes assuming that the entire payload is of l3 packet
		if len(payload) < 2:  # While l3 packet body can be empty, header must always be present (2 first bytes)
			print(f'[l3-decoder error]: invalid payload (length(payload={len(payload)})) was provided!')
			return None

		first_l3_header_byte: int = payload[0]
		second_l3_header_byte: int = payload[1]

		transfer_type: int = first_l3_header_byte & 0b00000011
		if transfer_type < 0 or transfer_type >= L3TransferType.COUNT:
			print(f'[l3-decoder error]: invalid payload transfer type (transfer_type={L3TransferType.to_string(transfer_type)}) was detected!')
			return None

		packet_type: int = (first_l3_header_byte >> 2) & 0b00001111
		if packet_type < 0 or packet_type >= L3PacketType.COUNT:
			print(f'[l3-decoder error]: invalid payload packet type (packet_type={L3PacketType.to_string(packet_type)}) was detected!')
			return None

		direction: int = (first_l3_header_byte >> 6) & 0b00000001
		if direction < 0 or direction >= L3PacketDirection.COUNT:
			print(f'[l3-decoder error]: invalid payload direction (direction={L3PacketDirection.to_string(direction)}) was detected!')
			return None

		status: int = (first_l3_header_byte >> 7) & 0b00000001
		if status < 0 or status >= L3PacketStatus.COUNT:
			print(f'[l3-decoder error]: invalid payload status (status={L3PacketStatus.to_string(status)}) was detected!')
			return None

		# NOTE: Body length checks are performed in the packet creation functions.

		if transfer_type == L3TransferType.BROADCAST:
			body_length: int = second_l3_header_byte
			body_data: bytearray = payload[2:]  # Omitting the l3 header
			return L3Packet.create_broadcast_packet(
				packet_type, direction, status, body_data, body_length
			)
		elif transfer_type == L3TransferType.UNICAST:
			recipient: int = payload[2]
			if recipient < 0 or status > 0xFF:
				print(f'[l3-decoder error]: non-l3 unicast recipient was decoded!')
				return None

			body_length: int = second_l3_header_byte
			body_data: bytearray = payload[3:]  # Omitting the l3 header
			L3Packet.create_unicast_packet(
				packet_type, direction, status, recipient, body_data, body_length
			)
		elif transfer_type == L3TransferType.MULTICAST:
			bottom_recipient: int = payload[2]
			if bottom_recipient < 0 or status > 0xFF:
				print(f'[l3-decoder error]: non-l3 unicast bottom_recipient was decoded!')
				return None

			top_recipient: int = payload[3]
			if top_recipient < 0 or status > 0xFF:
				print(f'[l3-decoder error]: non-l3 unicast top_recipient was decoded!')
				return None

			body_length: int = second_l3_header_byte
			body_data: bytearray = payload[4:]  # Omitting the l3 header
			L3Packet.create_multicast_packet(
				packet_type, direction, status, bottom_recipient, top_recipient, body_data, body_length
			)

	@staticmethod
	def print(l3_packet: L3Packet) -> None:
		assert l3_packet is not None, "[fatal]: provided l3_packet was None!"

		if l3_packet.header.transfer_type == L3TransferType.BROADCAST:
			template: str = f'''
L3Packet = [
	header = [
		transfer_type = {L3TransferType.to_string(l3_packet.header.transfer_type)},
		packet_type = {L3PacketType.to_string(l3_packet.header.packet_type)},
		direction = {L3PacketDirection.to_string(l3_packet.header.direction)},
		status = {L3PacketStatus.to_string(l3_packet.header.status)},
		body_length = {l3_packet.header.body_length}
	],
	body = [
		data = {l3_packet.inner.body.data}
	]
]
'''
		elif l3_packet.header.transfer_type == L3TransferType.UNICAST:
			template: str = f'''
L3Packet = [
	header = [
		transfer_type = {L3TransferType.to_string(l3_packet.header.transfer_type)},
		packet_type = {L3PacketType.to_string(l3_packet.header.packet_type)},
		direction = {L3PacketDirection.to_string(l3_packet.header.direction)},
		status = {L3PacketStatus.to_string(l3_packet.header.status)},
		body_length = {l3_packet.header.body_length}
	],
	body = [
		recipient = {l3_packet.inner.header.recipient},
		data = {l3_packet.inner.body.data}
	]
]
'''
		elif l3_packet.header.transfer_type == L3TransferType.MULTICAST:
			template: str = f'''
L3Packet = [
	header = [
		transfer_type = {L3TransferType.to_string(l3_packet.header.transfer_type)},
		packet_type = {L3PacketType.to_string(l3_packet.header.packet_type)},
		direction = {L3PacketDirection.to_string(l3_packet.header.direction)},
		status = {L3PacketStatus.to_string(l3_packet.header.status)},
		body_length = {l3_packet.header.body_length}
	],
	body = [
		bottom_recipient = {l3_packet.inner.header.bottom_recipient},
		top_recipient = {l3_packet.inner.header.top_recipient},
		data = {l3_packet.inner.body.data}
	]
]
'''
		else:
			assert False, "fatal: should never reach this block!"

		print(template)
