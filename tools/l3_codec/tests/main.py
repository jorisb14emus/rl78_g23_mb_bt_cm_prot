
from l2_codec.l2_decoder import L2Decoder
from l2_codec.l2_encoder import L2Encoder
from l2_codec.l2_packet import L2Packet, L2PacketType

from l3_codec.l3_decoder import L3Decoder
from l3_codec.l3_encoder import L3Encoder
from l3_codec.l3_packet import L3Packet, L3PacketType


def test(raw_data: bytearray) -> None:
	raw_data_slice: bytearray = raw_data

	while len(raw_data_slice) > 0:
		meta_data: bytearray = raw_data_slice[0:3]

		# Fetching SYNC_BYTE from stream
		first_byte: int = meta_data[0]
		if first_byte != 0x55:  # SYNC_BYTE
			raw_data_slice = raw_data_slice[1:]
			continue

		second_byte: int = meta_data[1]
		if second_byte <= 0:
			raw_data_slice = raw_data_slice[1:]
			continue

		if len(raw_data_slice) < second_byte + 3 + 2:
			raw_data_slice = raw_data_slice[1:]
			continue

		data: bytes = raw_data_slice[3:second_byte + second_byte]
		crc_bytes: bytes = raw_data_slice[len(data):len(data) + 2]
		payload: bytearray = bytearray(meta_data) + bytearray(data) + bytearray(crc_bytes)

		l2_packet: L2Packet = L2Decoder.decode(payload)

		if l2_packet is not None:
			# L2Decoder.print(l2_packet)
			raw_data_slice = raw_data_slice[len(payload):]

			raw_l2_packet: bytearray = L2Encoder.encode(l2_packet)
			# L2Encoder.print(raw_l2_packet, L2PacketType.RAW)
			if raw_l2_packet is None:
				continue

			l3_packet: L3Packet = L3Decoder.decode(raw_l2_packet[3:-2])
			if l3_packet is not None:
				L3Decoder.print(l3_packet)


def main():
	# payload: bytearray = bytearray(
	# 	# [0x55, 0x00, 0x02, 0x00, 0x00, 0x69]
	# 	# SYNC_BYTE   L2_LEN   SEQ_NO/L2_ID   TRNSFR_T/L3_ID/DIR/STATUS   L3_LEN  L3_DATA   CRC_BYTES
	# 	[    0x55,     0x03,    0b01001000,             0x00,             0x01,    69,     0x01, 0x02 ]
	# 	# [0b11000100, 0x04, 0x03, 0x04, 0x05, 0x06]
	# )

	payloads: list[bytearray] = [
		bytearray(
			[0x55, 0x03, 0b01001000, 0x00, 0x01, 69, 0x01, 0x02]
		),
		bytearray(
			[0x03, 0b01001000, 0x00, 0x01, 69, 0x01, 0x02]
		),
		bytearray(
			[0x55, 0x03, 0b01001000, 0x00, 0x01, 69, 0x01]
		),
		bytearray(
			[0x55, 0x03, 0b01001000, 0x01, 69, 0x01, 0x02]
		),
		bytearray(
			[0xFF, 0xEE, 0x55, 0x03, 0b01001000, 0x00, 0x01, 69, 0x01, 0x02, 0xAA, 0x01, 0x2]
		),
		bytearray(
			[0xFF, 0xEE, 0x55, 0x03, 0b01001000, 0x00, 0x01, 69, 0x01, 0x02, 0x55, 0x01, 0x2]
		)
	]

	for payload in payloads:
		print(f'+--------------------------------------------+')
		print(f'{payload}')
		test(payload)
	print(f'+--------------------------------------------+')

	# l3_packet: L3Packet = L3Decoder.decode(payload)
	# L3Decoder.print(l3_packet)
	# test_payload: bytearray = L3Encoder.encode(l3_packet)
	# L3Encoder.print(test_payload, L3PacketType.RAW)
	# L3Encoder.print(test_payload)
	# L3Encoder.print(test_payload, [16, 16])
	# L3Encoder.print(test_payload)
	# L3Encoder.print(test_payload, [8, 8, 8, 8])
	# L3Encoder.print(test_payload, [8, 8, 8])
	# L3Encoder.print(test_payload, [8, 8, 8, 8])
	# L3Encoder.print(test_payload, [8, 8])


if __name__ == '__main__':
	main()
