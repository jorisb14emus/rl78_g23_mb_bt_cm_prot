
from l2_codec.l2_packet import L2PacketBody

from typing import Any


class L3PacketDirection:
	FROM_BOTTOM_TO_TOP: int = 0
	FROM_TOP_TO_BOTTOM: int = 1
	COUNT: int = 2

	@staticmethod
	def to_string(direction: int) -> str:
		if direction == L3PacketDirection.FROM_BOTTOM_TO_TOP:
			return 'FROM_BOTTOM_TO_TOP'
		elif direction == L3PacketDirection.FROM_TOP_TO_BOTTOM:
			return 'FROM_TOP_TO_BOTTOM'
		else:
			assert '[fatal]: should never reach this block!'


class L3PacketStatus:
	UNTOUCHED: int = 0
	FLIPPED: int = 1
	COUNT: int = 2

	@staticmethod
	def to_string(status: int) -> str:
		if status == L3PacketStatus.UNTOUCHED:
			return 'UNTOUCHED'
		elif status == L3PacketStatus.FLIPPED:
			return 'FLIPPED'
		else:
			assert '[fatal]: should never reach this block!'


class L3PacketType:
	ENUMERATE_CELLS: int = 0
	GET_SERIAL_NUMBER: int = 1
	GET_SW_VERSION: int = 2
	GET_HW_STATS: int = 3
	CHANGE_RUNNING_PARTITION: int = 4
	RAW: int = 5
	COUNT: int = 6

	@staticmethod
	def get_bits_steps(packet_type: int) -> list[int] | None:
		if packet_type is None:
			return None

		if packet_type == L3PacketType.ENUMERATE_CELLS:
			return [8, 1]
		elif packet_type == L3PacketType.GET_SERIAL_NUMBER:
			pass
		elif packet_type == L3PacketType.GET_SW_VERSION:
			pass
		elif packet_type == L3PacketType.GET_HW_STATS:
			pass
		elif packet_type == L3PacketType.CHANGE_RUNNING_PARTITION:
			pass
		elif packet_type == L3PacketType.RAW:
			return None
		else:
			return None

	@staticmethod
	def to_string(packet_type: int) -> str:
		if packet_type == L3PacketType.ENUMERATE_CELLS:
			return 'ENUMERATE_CELLS'
		elif packet_type == L3PacketType.GET_SERIAL_NUMBER:
			return 'GET_SERIAL_NUMBER'
		elif packet_type == L3PacketType.GET_SW_VERSION:
			return 'GET_SW_VERSION'
		elif packet_type == L3PacketType.GET_HW_STATS:
			return 'GET_HW_STATS'
		elif packet_type == L3PacketType.CHANGE_RUNNING_PARTITION:
			return 'CHANGE_RUNNING_PARTITION'
		elif packet_type == L3PacketType.RAW:
			return 'RAW'
		else:
			assert '[fatal]: should never reach this block!'


class L3TransferType:
	BROADCAST: int = 0
	UNICAST: int = 1
	MULTICAST: int = 2
	COUNT: int = 3

	@staticmethod
	def to_string(transfer_type: int) -> str:
		if transfer_type == L3TransferType.BROADCAST:
			return 'BROADCAST'
		elif transfer_type == L3TransferType.UNICAST:
			return 'UNICAST'
		elif transfer_type == L3TransferType.MULTICAST:
			return 'MULTICAST'
		else:
			assert '[fatal]: should never reach this block!'


class L3PacketHeader:
	SIZEOF_SELF: int = 2

	def __init__(
			self,
			transfer_type: int,
			packet_type: int,
			direction: int,
			status: int,
			body_length: int) -> None:
		self.transfer_type: int = transfer_type
		self.packet_type: int = packet_type
		self.direction: int = direction
		self.status: int = status
		self.body_length: int = body_length


class L3BroadcastPacketHeader:
	pass


class L3BroadcastPacketBody:
	def __init__(self, payload: bytearray) -> None:
		self.data: bytearray = payload


class L3BroadcastPacket:
	def __init__(self, payload: bytearray) -> None:
		self.header: L3BroadcastPacketHeader = L3BroadcastPacketHeader()
		self.body: L3BroadcastPacketBody = L3BroadcastPacketBody(payload)


class L3UnicastPacketHeader:
	SIZEOF_SELF: int = 1

	def __init__(self, recipient: int):
		self.recipient: int = recipient


class L3UnicastPacketBody:
	def __init__(self, payload: bytearray) -> None:
		self.data: bytearray = payload


class L3UnicastPacket:
	def __init__(self, recipient: int, payload: bytearray) -> None:
		self.header: L3UnicastPacketHeader = L3UnicastPacketHeader(recipient)
		self.body: L3UnicastPacketBody = L3UnicastPacketBody(payload)


class L3MulticastPacketHeader:
	SIZEOF_SELF: int = 2

	def __init__(self, bottom_recipient: int, top_recipient: int):
		self.bottom_recipient: int = bottom_recipient
		self.top_recipient: int = top_recipient


class L3MulticastPacketBody:
	def __init__(self, payload: bytearray) -> None:
		self.data: bytearray = payload


class L3MulticastPacket:
	def __init__(self, bottom_recipient: int, top_recipient: int, payload: bytearray) -> None:
		self.header: L3MulticastPacketHeader = L3MulticastPacketHeader(bottom_recipient, top_recipient)
		self.body: L3MulticastPacketBody = L3MulticastPacketBody(payload)


class L3Packet:
	def __init__(
			self,
			transfer_type: int,
			packet_type: int,
			direction: int,
			status: int,
			body_length: int) -> None:
		self.header: L3PacketHeader = L3PacketHeader(
			transfer_type,
			packet_type,
			direction,
			status,
			body_length
		)

		self.inner: L3BroadcastPacket | L3UnicastPacket | L3MulticastPacket | None = None

	@staticmethod
	def _create_meta_packet(
			transfer_type: int,
			packet_type: int,
			direction: int,
			status: int) -> 'L3Packet':
		l3_packet: L3Packet = L3Packet(
			transfer_type, packet_type, direction, status, 0)
		return l3_packet

	@staticmethod
	def create_broadcast_packet(
			packet_type: int,
			direction: int,
			status: int,
			payload: bytearray,
			payload_length: int) -> Any:
		l3_packet: L3Packet = L3Packet._create_meta_packet(
			L3TransferType.BROADCAST, packet_type, direction, status
		)

		if len(payload) < payload_length or payload_length <= 0 or payload_length > (L2PacketBody.SIZEOF_SELF - L3PacketHeader.SIZEOF_SELF):
			print(f'[l3-packet error]: invalid payload or payload_length provided while creating broadcast packet!')
			return None

		l3_packet.inner = L3BroadcastPacket(payload)
		l3_packet.header.body_length = payload_length
		return l3_packet

	@staticmethod
	def create_unicast_packet(
			packet_type: int,
			direction: int,
			status: int,
			recipient: int,
			payload: bytearray,
			payload_length: int) -> Any:
		l3_packet: L3Packet = L3Packet._create_meta_packet(
			L3TransferType.UNICAST, packet_type, direction, status
		)

		if len(payload) < payload_length or payload_length <= 0 or payload_length > (L2PacketBody.SIZEOF_SELF - L3PacketHeader.SIZEOF_SELF - L3UnicastPacketHeader.SIZEOF_SELF):
			print(f'[l3-packet error]: invalid payload or payload_length provided while creating unicast packet!')
			return None

		l3_packet.inner = L3UnicastPacket(recipient, payload)
		l3_packet.header.body_length = payload_length
		return l3_packet

	@staticmethod
	def create_multicast_packet(
			packet_type: int,
			direction: int,
			status: int,
			bottom_recipient: int,
			top_recipient: int,
			payload: bytearray,
			payload_length: int) -> Any:
		l3_packet: L3Packet = L3Packet._create_meta_packet(
			L3TransferType.MULTICAST, packet_type, direction, status
		)

		if len(payload) < payload_length or payload_length <= 0 or payload_length > (L2PacketBody.SIZEOF_SELF - L3PacketHeader.SIZEOF_SELF - L3MulticastPacketHeader.SIZEOF_SELF):
			print(f'[l3-packet error]: invalid payload or payload_length provided while creating multicast packet!')
			return None

		l3_packet.inner = L3MulticastPacket(bottom_recipient, top_recipient, payload)
		l3_packet.header.body_length = payload_length
		return l3_packet
