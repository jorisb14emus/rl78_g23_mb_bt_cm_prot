
from l2_codec.l2_decoder import L2Decoder
from l2_codec.l2_encoder import L2Encoder
from l2_codec.l2_packet import L2Packet, L2PacketType

from l3_codec.l3_decoder import L3Decoder
from l3_codec.l3_encoder import L3Encoder
from l3_codec.l3_packet import L3Packet

import serial


class SerialUsbTransceiver:
	_serial_port: serial.Serial | None = None
	_sequence_number: int = 0

	@staticmethod
	def bind(
			serial_id: str,
			baud_rate: int,
			timeout: float | None = None,
			parity: int = serial.PARITY_NONE,
			stop_bits: int = serial.STOPBITS_ONE,
			byte_size: int = serial.EIGHTBITS):
		SerialUsbTransceiver._serial_port = serial.Serial(serial_id)
		SerialUsbTransceiver._serial_port.baudrate = baud_rate
		SerialUsbTransceiver._serial_port.timeout = timeout
		SerialUsbTransceiver._serial_port.parity = parity
		SerialUsbTransceiver._serial_port.stopbits = stop_bits
		SerialUsbTransceiver._serial_port.bytesize = byte_size

		try:
			if not SerialUsbTransceiver._serial_port.is_open:
				SerialUsbTransceiver._serial_port.open()
		except serial.SerialException as message:
			print(f'[serial-usb-transceiver error]: failed to open serial port `{serial_id}` with an error - `{message}`!')
			exit(-1)

	@staticmethod
	def receive_l2_packet() -> L2Packet | None:
		if not SerialUsbTransceiver._serial_port.is_open:
			print(f'[serial-usb-transceiver error]: provided serial port is not opened!')
			return None

		while True:
			while SerialUsbTransceiver._serial_port.inWaiting() < 3:
				pass

			meta_data: bytes = SerialUsbTransceiver._serial_port.read(3)

			if len(meta_data) != 3:
				return None

			# Fetching SYNC_BYTE from stream
			sync_byte: int = meta_data[0]
			if sync_byte != 0x55:  # SYNC_BYTE
				continue

			body_length: int = meta_data[1]
			if body_length < 0:
				continue

			# NOTE: Adding 2 for crc bytes
			while SerialUsbTransceiver._serial_port.inWaiting() < body_length + 2:
				continue

			# NOTE: Adding 2 for crc bytes
			data: bytes = SerialUsbTransceiver._serial_port.read(body_length)

			if len(data) != body_length:
				return None

			crc_bytes: bytes = SerialUsbTransceiver._serial_port.read(2)
			payload: bytearray = \
				bytearray(meta_data) + \
				bytearray(data) + \
				bytearray(crc_bytes)

			l2_packet: L2Packet = L2Decoder.decode(payload)
			return l2_packet

	@staticmethod
	def receive_l3_packet() -> L3Packet | None:
		l2_packet: L2Packet | None = SerialUsbTransceiver.receive_l2_packet()
		if l2_packet is None:
			print(f'[serial-usb-transceiver error]: aborting l3 packet reception due to invalid l2 packet!')
			return None

		if l2_packet.header.packet_type != L2PacketType.RAW:
			print(f'[serial-usb-transceiver info]:  skipping any l2 packet that are not of type RAW (non-l3 packets)!')
			return None

		raw_l2_packet: bytearray = L2Encoder.encode(l2_packet)
		if raw_l2_packet is None:
			print(f'[serial-usb-transceiver error]: aborting l3 packet reception due to failed l2 packet encoding!')
			return None

		# Removing SYNC_BYTE, L2 header, and crc bytes for the L3 packet
		l3_packet: L3Packet = L3Decoder.decode(raw_l2_packet[3:-2])

		# if l3_packet is not None:
		# 	ack_packet: L2Packet = L2Packet.create_ack_packet(l2_packet.header.sequence_number)
		# 	SerialUsbTransceiver.transmit_l2_packet(ack_packet)

		return l3_packet

	@staticmethod
	def transmit_l2_packet(l2_packet: L2Packet) -> None:
		payload: bytearray = L2Encoder.encode(l2_packet)
		if payload is None:
			print(f'[serial-usb-transmitter error]: cannot to send l2 packet due to failed encode call on that packet!')
			return

		SerialUsbTransceiver._serial_port.write(payload)

	@staticmethod
	def transmit_l3_packet(l3_packet: L3Packet) -> None:
		payload: bytearray = L3Encoder.encode(l3_packet)
		if payload is None:
			print(f'[serial-usb-transmitter error]: cannot to send l3 packet due to failed encode call on that packet!')
			return

		# TODO: handle the sequence number here!
		l2_packet: L2Packet = L2Packet.create_raw_packet(
			SerialUsbTransceiver._sequence_number, payload, len(payload)
		)

		SerialUsbTransceiver._sequence_number += 1
		if SerialUsbTransceiver._sequence_number > 7:
			SerialUsbTransceiver._sequence_number = 0

		SerialUsbTransceiver.transmit_l2_packet(l2_packet)
