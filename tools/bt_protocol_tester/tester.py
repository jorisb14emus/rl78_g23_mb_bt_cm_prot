
from l3_codec.l3_decoder import L3Decoder
from l3_codec.l3_packet import L3Packet, L3PacketType, L3PacketDirection, L3PacketStatus


def process_user_input() -> L3Packet | None:
	choice = int(input('select a packet to send:\n'
					   '1.  enumerate cells\n'
					   '2.  get serial number\n'
					   '3.  custom testing packet\n'
					   '> '))
	if choice == 1:
		l3_packet: L3Packet = L3Packet.create_broadcast_packet(
			L3PacketType.ENUMERATE_CELLS,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			bytearray([0x00, 0x01, 0x00, 0x01]),
			4
		)
	elif choice == 2:
		l3_packet: L3Packet = L3Packet.create_unicast_packet(
			L3PacketType.GET_SERIAL_NUMBER,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			0,
			bytearray([0x00, 0x01, 0x02]),
			3
		)
	elif choice == 3:
		l3_packet: L3Packet = L3Packet.create_multicast_packet(
			L3PacketType.GET_SERIAL_NUMBER,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			0,
			0,
			bytearray([0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A]),
			10
		)
	elif choice == 4:
		l3_packet: L3Packet = L3Packet.create_broadcast_packet(
			L3PacketType.ENUMERATE_CELLS,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			bytearray([0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A]),
			13
		)
	elif choice == 5:
		l3_packet: L3Packet = L3Packet.create_unicast_packet(
			L3PacketType.ENUMERATE_CELLS,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			0,
			bytearray([0x00]),
			1
		)
	elif choice == 6:
		l3_packet: L3Packet = L3Packet.create_multicast_packet(
			L3PacketType.ENUMERATE_CELLS,
			L3PacketDirection.FROM_BOTTOM_TO_TOP,
			L3PacketStatus.UNTOUCHED,
			0,
			0,
			bytearray([0x00]),
			1
		)
	else:
		return None

	L3Decoder.print(l3_packet)
	return l3_packet
