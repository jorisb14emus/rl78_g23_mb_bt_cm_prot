
from l2_codec.l2_decoder import L2Decoder
from l2_codec.l2_encoder import L2Encoder
from l2_codec.l2_packet import L2Packet, L2PacketType


def main():
	l2_packet: L2Packet = L2Packet.create_raw_packet(3, 0, bytearray([69]), 1)
	L2Decoder.print(l2_packet)

	payload: bytearray = L2Encoder.encode(l2_packet)
	L2Encoder.print(payload, L2PacketType.RAW)

	# L2Decoder.decode(payload)

	# payload: bytearray = bytearray(
	# 	[0x55, 0x01, 0b01001000, 69]
	# 	# [0b11000100, 0x04, 0x03, 0x04, 0x05, 0x06]
	# )


if __name__ == '__main__':
	main()
