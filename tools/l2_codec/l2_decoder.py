
from l2_codec.l2_packet import L2Packet, L2PacketType, L2PacketBody


class L2Decoder:
	@staticmethod
	def decode(payload: bytearray) -> L2Packet | None:
		assert payload is not None, "[fatal]: provided payload was None!"

		if len(payload) < 5:  # While l2 packet body can be empty, SYNC_BYTE and header must always be present (3 first bytes)
			print(f'[l2-decoder error]: invalid payload (length(payload={len(payload)})) was provided!')
			return None

		first_l2_header_byte: int = payload[0]
		second_l2_header_byte: int = payload[1]
		third_l2_header_byte: int = payload[2]

		sync_byte: int = first_l2_header_byte
		if sync_byte != 0x55:
			print(f'[l2-decoder error]: invalid payload (payload[0] != SYNC_BYTE) was provided!')
			return None

		body_length: int = second_l2_header_byte
		if body_length < 0 or body_length > L2PacketBody.SIZEOF_SELF:
			print(f'[l2-decoder error]: invalid payload body length (payload[1] < 0 or payload[1] > L2PacketBody.SIZEOF_SELF)!')
			return None

		sequence_number: int = third_l2_header_byte & 0b00000111
		if sequence_number < 0 or sequence_number > 0b00000111:
			print(f'[l2-decoder error]: invalid payload sequence number (sequence_number={sequence_number}) was detected!')
			return None

		packet_type: int = (third_l2_header_byte >> 3) & 0b00011111
		if packet_type < 0 or packet_type > L2PacketType.COUNT:
			print(f'[l2-decoder error]: invalid payload packet type (sequence_number={L2PacketType.to_string(packet_type)}) was detected!')
			return None

		# NOTE: Body length checks are performed in the packet creation functions.
		body_data: bytearray = payload[3:-2]  # Omitting the SYNC_BYTE, l2 header, and crc bytes

		if packet_type == L2PacketType.PING:
			return L2Packet.create_ping_packet(sequence_number)
		elif packet_type == L2PacketType.ACK:
			return L2Packet.create_ack_packet(sequence_number)
		elif packet_type == L2PacketType.NACK:
			return L2Packet.create_nack_packet(sequence_number)
		elif packet_type == L2PacketType.RAW:
			return L2Packet.create_raw_packet(sequence_number, body_data, body_length)
		else:
			print(f'[l2-decoder error]: unknown l2 packet type was encountered (packet_type={packet_type})!')
			return None

	@staticmethod
	def print(l2_packet: L2Packet) -> None:
		assert l2_packet is not None, "[fatal]: provided l2_packet was None!"
		template: str = f'''
L2Packet = [
	header = [
		packet_type = {L2PacketType.to_string(l2_packet.header.packet_type)},
		sequence_number = {l2_packet.header.sequence_number},
		body_length = {l2_packet.header.body_length}
	],
	body = [
		data = {l2_packet.body.data}
	]
]
'''
		print(template)
