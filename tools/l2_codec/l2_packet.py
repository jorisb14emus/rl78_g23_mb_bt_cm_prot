
from crc import Configuration, Calculator

from typing import Any


class L2PacketType:
	PING: int = 0
	ACK: int = 1
	NACK: int = 2
	RAW: int = 9
	COUNT: int = 10

	@staticmethod
	def get_bits_steps(packet_type: int) -> list[int] | None:
		if packet_type is None:
			return None

		if packet_type == L2PacketType.PING:
			return []
		elif packet_type == L2PacketType.ACK:
			return []
		elif packet_type == L2PacketType.NACK:
			return []
		else:
			return None

	@staticmethod
	def to_string(packet_type: int) -> str:
		if packet_type == L2PacketType.PING:
			return 'PING'
		elif packet_type == L2PacketType.ACK:
			return 'ACK'
		elif packet_type == L2PacketType.NACK:
			return 'NACK'
		elif packet_type == L2PacketType.RAW:
			return 'RAW'
		else:
			assert "[fatal]: should never reach this block!"


class L2PacketHeader:
	SIZEOF_SELF: int = 2

	def __init__(
			self,
			packet_type: int,
			sequence_number: int,
			body_length: int,
			crc_bytes: int) -> None:
		self.packet_type: int = packet_type
		self.sequence_number: int = sequence_number
		self.body_length: int = body_length
		self.crc_bytes: int = crc_bytes


class L2PacketBody:
	SIZEOF_SELF: int = 64

	def __init__(self, payload: bytearray) -> None:
		self.data: bytearray = payload


class L2Packet:
	def __init__(
			self,
			packet_type: int,
			sequence_number: int,
			body_length: int,
			crc_bytes: int) -> None:
		self.header: L2PacketHeader = L2PacketHeader(
			packet_type,
			sequence_number,
			body_length,
			crc_bytes
		)

		self.body: L2PacketBody = L2PacketBody(bytearray())

	@staticmethod
	def _ccitt_kermit(l2_packet: 'L2Packet') -> int:
		first_byte = \
			l2_packet.header.sequence_number + \
			(l2_packet.header.packet_type << 3)

		payload: bytearray = bytearray([
			first_byte
		]) + l2_packet.body.data

		configuration: Configuration = Configuration(
			width=16,
			polynomial=0x1021,
			init_value=0x0000,
			final_xor_value=0x00,
			reverse_input=True,
			reverse_output=True,
		)

		calculator: Calculator = Calculator(configuration)
		return calculator.checksum(payload)

	@staticmethod
	def _create_meta_packet(packet_type: int, sequence_number: int) -> 'L2Packet':
		l2_packet: L2Packet = L2Packet(
			packet_type, sequence_number, 0, 0)
		return l2_packet

	@staticmethod
	def create_ping_packet(sequence_number: int) -> 'L2Packet':
		l2_packet: L2Packet = L2Packet._create_meta_packet(
			L2PacketType.PING, sequence_number
		)
		# NOTE: not checking the length, since PING is always 5 bytes with SYNC_BYTE and crc bytes
		l2_packet.header.crc_bytes = L2Packet._ccitt_kermit(l2_packet)
		return l2_packet

	@staticmethod
	def create_ack_packet(sequence_number: int) -> 'L2Packet':
		l2_packet: L2Packet = L2Packet._create_meta_packet(
			L2PacketType.ACK, sequence_number
		)
		# NOTE: not checking the length, since ACK is always 5 bytes with SYNC_BYTE and crc bytes
		l2_packet.header.crc_bytes = L2Packet._ccitt_kermit(l2_packet)
		return l2_packet

	@staticmethod
	def create_nack_packet(sequence_number: int) -> 'L2Packet':
		l2_packet: L2Packet = L2Packet._create_meta_packet(
			L2PacketType.NACK, sequence_number
		)
		# NOTE: not checking the length, since NACK is always 5 bytes with SYNC_BYTE and crc bytes
		l2_packet.header.crc_bytes = L2Packet._ccitt_kermit(l2_packet)
		return l2_packet

	@staticmethod
	def create_raw_packet(
			sequence_number: int,
			payload: bytearray,
			payload_length: int) -> Any:
		l2_packet: L2Packet = L2Packet._create_meta_packet(
			L2PacketType.RAW, sequence_number
		)

		if len(payload) < payload_length or payload_length <= 0 or payload_length > L2PacketBody.SIZEOF_SELF:
			print(f'[l2-packet error]: invalid payload or payload_length provided while creating unicast packet!')
			return None

		l2_packet.body.data = payload
		l2_packet.header.body_length = payload_length
		l2_packet.header.crc_bytes = L2Packet._ccitt_kermit(l2_packet)
		return l2_packet
