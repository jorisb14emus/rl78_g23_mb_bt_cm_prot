
from l2_codec.l2_packet import L2Packet, L2PacketType


class L2Encoder:
	@staticmethod
	def encode(l2_packet: L2Packet) -> bytearray | None:
		assert l2_packet is not None, "[fatal]: provided l2_packet was None!"

		packet_type: int = l2_packet.header.packet_type & 0b00011111
		sequence_number: int = l2_packet.header.sequence_number & 0b00000111
		body_length: int = l2_packet.header.body_length
		body_data: bytearray = l2_packet.body.data

		first_l2_header_byte: int = 0x55
		second_l2_header_byte: int = body_length
		third_l2_header_byte: int = \
			(sequence_number << 0) + \
			(packet_type     << 3)

		payload: bytearray = bytearray([
			first_l2_header_byte, second_l2_header_byte, third_l2_header_byte,  # L2 header bytes
		]) + body_data + bytearray([
			l2_packet.header.crc_bytes & 0b11111111,
			(l2_packet.header.crc_bytes >> 8) & 0b11111111
		])

		return payload

	@staticmethod
	def print(payload: bytearray, packet_type: int | None = None, end_line: str = '\n') -> None:
		assert payload is not None, "[fatal]: provided payload was None!"

		def bit_mask(offset: int, bit_count: int) -> int:
			return ((0x01 << bit_count) - 1) << offset

		def read_value(payload: bytearray, bits_in_value: int, bits_offset: int) -> int:
			byte_offset: int = bits_offset // 8
			bit_offset: int = bits_offset % 8
			current_value: int = int.from_bytes(payload[byte_offset:byte_offset + 4], byteorder='little')
			shifted_right_value: int = current_value >> bit_offset
			masked_value: int = shifted_right_value & bit_mask(0, bits_in_value)
			return masked_value

		if packet_type is None:
			count: int = len(payload) - 2
			bits_steps: list[int] = [8] * count
		else:
			bits_steps: list[int] = L2PacketType.get_bits_steps(packet_type)
			if bits_steps is None:
				count: int = len(payload) - 3
				bits_steps: list[int] = [8] * count

		temp_list: list[int] = [8, 8, 3, 5]
		temp_list.extend(bits_steps)
		bits_steps = temp_list

		print(f'length:  ', end=' ')
		for step in bits_steps:
			string: str = f'Bits ({step})'
			string = string.ljust(step if step > 8 else 8)
			print(f'{string}', end=' ')
		print()

		print(f'binary:  ', end=' ')
		bits_offset: int = 0
		for index, step in enumerate(bits_steps):
			value: int = read_value(payload, step, bits_offset)
			bits_offset += step
			string: str = f'{value:0{step}b}'
			string = string.ljust(8)
			print(f'{string}', end=' ')
		print()

		print(f'decimal: ', end=' ')
		bits_offset: int = 0
		for index, step in enumerate(bits_steps):
			value: int = read_value(payload, step, bits_offset)
			bits_offset += step
			string: str = f'{value}'.ljust(step if step > 8 else 8)
			print(f'{string}', end=' ')
		print(f'{end_line}')
