
/*
 * ll_leds.c
 *
 * Created on: 13 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms)
 */

//////////////////////////
// INCLUDES
//
#include "ll_leds/ll_leds.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"

#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// PRIVATE TYPES AND MACROS
//
typedef struct
{
	LL_LedType_t type;
	volatile uint8_t* address;
	uint8_t mask;
	bool inverted;
} LL_LedDescriptor_t;



////////////////////////////
// PRIVATE ATTRIBUTES
//
static LL_LedDescriptor_t ledBindings_[LL_LED_TYPES_COUNT] =
{
	[LL_LED_TYPE_GREEN] =
	{
		LL_LED_TYPE_GREEN, &P7, 0b00010000, true
	},
	[LL_LED_TYPE_RED] =
	{
		LL_LED_TYPE_RED, &P1, 0b01000000, false
	}
};



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void LL_Leds_init(void)
{
	for (uint8_t index = 0; index < LL_LED_TYPES_COUNT; ++index)
	{
		LL_Led_set(ledBindings_[index].type, LL_LED_VALUE_LOW);
	}
}

void LL_Led_toggle(const LL_LedType_t led)
{
	const uint8_t value = *(ledBindings_[led].address);
	const uint8_t mask = ledBindings_[led].mask;
	*(ledBindings_[led].address) = ~value & mask;
}

void LL_Led_set(const LL_LedType_t led, const LL_LedValue_t value)
{
	const uint8_t mask = ledBindings_[led].mask;
	const bool inverted = ledBindings_[led].inverted;
	*(ledBindings_[led].address) = inverted ? (~value & mask) : (value & mask);
}
