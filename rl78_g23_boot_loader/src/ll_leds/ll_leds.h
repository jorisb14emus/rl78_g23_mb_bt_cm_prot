
/*
 * ll_leds.h
 *
 * Created on: 13 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms)
 */

#ifndef __LL_LEDS__LL_LEDS_H__
#define __LL_LEDS__LL_LEDS_H__

//////////////////////////
// INCLUDES
//
typedef enum
{
	LL_LED_TYPE_GREEN = 0,
	LL_LED_TYPE_RED,
	LL_LED_TYPES_COUNT
} LL_LedType_t;

typedef enum
{
	LL_LED_VALUE_LOW = 0x00,
	LL_LED_VALUE_HIGH = 0xFF,
	LL_LED_VALUES_COUNT
} LL_LedValue_t;



////////////////////////////
// TYPES AND MACROS
//



////////////////////////////
// PUBLIC INTERFACE
//
void LL_Leds_init(void);

void LL_Led_toggle(const LL_LedType_t led);

void LL_Led_set(const LL_LedType_t led, const LL_LedValue_t value);

#endif /* __LL_LEDS__LL_LEDS_H__ */
