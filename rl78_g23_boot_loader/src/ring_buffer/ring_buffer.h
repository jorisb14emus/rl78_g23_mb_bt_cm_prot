
/*
 * ring_buffer.h
 *
 * Created on: 2 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_RING_BUFFER__LL_RING_BUFFER_H__
#define __LL_RING_BUFFER__LL_RING_BUFFER_H__

//////////////////////////
// INCLUDES
//
#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
#define DEFINE_RING_BUFFER(_typeName, _elementType, _capacity) \
	typedef struct                                             \
	{                                                          \
		_elementType data[_capacity];                          \
		uint8_t head;                                          \
		uint8_t tail;                                          \
	} _typeName ## _t;                                         \
	                                                           \
	void _typeName ## _create(                                 \
		_typeName ## _t* const ringBuffer);                    \
	                                                           \
	void _typeName ## _create(                                 \
		_typeName ## _t* const ringBuffer)                     \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		ringBuffer->head = 0;                                  \
		ringBuffer->tail = 0;                                  \
	}                                                          \
	                                                           \
	bool _typeName ## _give(                                   \
		_typeName ## _t* const ringBuffer,                     \
		const _elementType element);                           \
	                                                           \
	bool _typeName ## _give(                                   \
		_typeName ## _t* const ringBuffer,                     \
		const _elementType element)                            \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		uint8_t next = ringBuffer->head + 1;                   \
		                                                       \
		if (next >= (_capacity))                               \
		{                                                      \
			next = 0;                                          \
		}                                                      \
		                                                       \
		if (next == ringBuffer->tail)                          \
		{                                                      \
			return false;                                      \
		}                                                      \
		                                                       \
		ringBuffer->data[ringBuffer->head] = element;          \
		ringBuffer->head = next;                               \
		return true;                                           \
	}                                                          \
	                                                           \
	bool _typeName ## _peek(                                   \
		_typeName ## _t* const ringBuffer,                     \
		_elementType* const element);                          \
	                                                           \
	bool _typeName ## _peek(                                   \
		_typeName ## _t* const ringBuffer,                     \
		_elementType* const element)                           \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		DEBUG_ASSERT(element);                                 \
		                                                       \
		if (ringBuffer->head == ringBuffer->tail)              \
		{                                                      \
			return false;                                      \
		}                                                      \
		                                                       \
		uint8_t next = ringBuffer->tail + 1;                   \
		if(next >= (_capacity)) next = 0;                      \
		                                                       \
		*element = ringBuffer->data[ringBuffer->tail];         \
		return true;                                           \
	}                                                          \
	                                                           \
	bool _typeName ## _take(                                   \
		_typeName ## _t* const ringBuffer,                     \
		_elementType* const element);                          \
	                                                           \
	bool _typeName ## _take(                                   \
		_typeName ## _t* const ringBuffer,                     \
		_elementType* const element)                           \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		DEBUG_ASSERT(element);                                 \
		                                                       \
		if (ringBuffer->head == ringBuffer->tail)              \
		{                                                      \
			return false;                                      \
		}                                                      \
		                                                       \
		uint8_t next = ringBuffer->tail + 1;                   \
		if(next >= (_capacity)) next = 0;                      \
		                                                       \
		*element = ringBuffer->data[ringBuffer->tail];         \
		ringBuffer->tail = next;                               \
		return true;                                           \
	}                                                          \
	                                                           \
	bool _typeName ## _isEmpty(                                \
		_typeName ## _t* const ringBuffer);                    \
	                                                           \
	bool _typeName ## _isEmpty(                                \
		_typeName ## _t* const ringBuffer)                     \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		return (ringBuffer->head == ringBuffer->tail);         \
	}                                                          \
	                                                           \
	bool _typeName ## _isFull(                                 \
		_typeName ## _t* const ringBuffer);                    \
	                                                           \
	bool _typeName ## _isFull(                                 \
		_typeName ## _t* const ringBuffer)                     \
	{                                                          \
		DEBUG_ASSERT(ringBuffer);                              \
		uint8_t next = ringBuffer->head + 1;                   \
		                                                       \
		if (next >= (_capacity))                               \
		{                                                      \
			next = 0;                                          \
		}                                                      \
		                                                       \
		return (next == ringBuffer->tail);                     \
	}                                                          \



//////////////////////////
// PUBLIC INTERFACE
//

#endif /* __LL_RING_BUFFER__LL_RING_BUFFER_H__ */
