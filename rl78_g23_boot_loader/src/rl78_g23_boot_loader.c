
/*
 * rl78_g23_boot_loader.c
 *
 * Created on: 7 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "linker_config.h"

#include "debug/asserts.h"
#include "ll_crc/ll_crc.h"
#include "ll_time/ll_time.h"
#include "ll_nvm/ll_nvm.h"

#include "l3/l3_packet.h"
#include "l3/l3_network.h"

#include "events/events.h"
#include "dispatcher/dispatcher.h"
#include "updaters/boot_loader_updater.h"
#include "updaters/application_updater.h"

#include "r_smc_entry.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//
#define WAIT_TIME_IN_MS 5000

enum
{
	STATE_TYPE_IDLING,
	STATE_TYPE_PROCESSING_PROTOCOL,
	STATE_TYPE_UPDATING_BOOT_LOADER,
	STATE_TYPE_UPDATING_APPLICATION,
	STATE_TYPES_COUNT
};

typedef uint8_t StateType_t;

typedef struct
{
	void(*enter)(void); // Can be NULL
	void(*run)(EventHandle_t event); // Must not be NULL!
	void(*exit)(void); // Can be NULL
} State_t;

typedef State_t* StateHandle_t;



////////////////////////////
// PRIVATE ATTRIBUTES
//
static LL_Time_t time0_;
static bool stayInBootLoader_;
static State_t states_[STATE_TYPES_COUNT];
static StateType_t currentState_;



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
signed int Main_bootLoaderInvoke(void);

static void initStates_(void);
static void transitionState_(const StateType_t stateType);
static void processEvents_(void);

static void Idle_enter(void);
static void Idle_run(EventHandle_t event);
static void Idle_exit(void);



////////////////////////////
// IMPLEMENTATION
//
signed int Main_bootLoaderInvoke(void)
{
	EI();

	LL_Crc_init();
	LL_Time_init();
	LL_Nvm_init(LL_NVM_FREQUENCY_32MHz);

	Dispatcher_init();

	initStates_();
	transitionState_(STATE_TYPE_IDLING);

	stayInBootLoader_ = false;
	time0_ = LL_Time_getMs();

	// TODO: remove:
	stayInBootLoader_ = true;

// TODO: remove:
// [
	L3_Packet_t packet;
	packet.header.transferType = L3_TRANSFER_TYPE_UNICAST;
	packet.header.packetType = 10;
	packet.header.direction = 0;
	packet.header.status = 0;
	packet.header.bodyLength = 5;
	packet.unicastPacket.header.recipient = 0;
	packet.unicastPacket.body.data[0] = 9;
	packet.unicastPacket.body.data[1] = 4;
	packet.unicastPacket.body.data[2] = 4;
	packet.unicastPacket.body.data[3] = 4;
	packet.unicastPacket.body.data[4] = 9;

	(void)EventQueue_give((Event_t)
	{
		.type = EVENT_TYPE_RECEIVED_APPLICATION_UPDATE_BLOCK,
		.l3Packet = packet
	});
// ]

	while (stayInBootLoader_ || LL_Time_getElapsedMs(time0_) < WAIT_TIME_IN_MS)
	{
		Dispatcher_listen();
		processEvents_();
	}

	return 0;
}

static void initStates_(void)
{
	states_[STATE_TYPE_IDLING] = (State_t)
	{
		.enter = Idle_enter,
		.run = Idle_run,
		.exit = Idle_exit
	};

	states_[STATE_TYPE_PROCESSING_PROTOCOL] = (State_t)
	{
		.enter = Dispatcher_enter,
		.run = Dispatcher_run,
		.exit = Dispatcher_exit
	};

	states_[STATE_TYPE_UPDATING_BOOT_LOADER] = (State_t)
	{
		.enter = BootLoaderUpdater_enter,
		.run = BootLoaderUpdater_run,
		.exit = BootLoaderUpdater_exit
	};

	states_[STATE_TYPE_UPDATING_APPLICATION] = (State_t)
	{
		.enter = ApplicationUpdater_enter,
		.run = ApplicationUpdater_run,
		.exit = ApplicationUpdater_exit
	};
}

static void transitionState_(const StateType_t stateType)
{
	// The exit routine of current state before transitioning.
	StateHandle_t oldState = &states_[currentState_];
	if (oldState->exit) oldState->exit();

	currentState_ = stateType;

	// The enter routine of a "transition-to" state.
	StateHandle_t newState = &states_[currentState_];
	if (newState->enter) newState->enter();
}

static void processEvents_(void)
{
	Event_t event = EventQueue_take();

	if (event.type != EVENT_TYPE_NONE)
	{
		stayInBootLoader_ = true; // Stay in the boot loader flag
		StateHandle_t state = &states_[currentState_];
		DEBUG_ASSERT(state->run);
		state->run(&event);
	}
}

static void Idle_enter(void)
{
}

static void Idle_run(EventHandle_t event)
{
	switch (event->type)
	{
		case EVENT_TYPE_DISPATCH_PACKET:
		{
			transitionState_(STATE_TYPE_PROCESSING_PROTOCOL);
			(void)EventQueue_give(*event);
		} break;

		case EVENT_TYPE_RECEIVED_APPLICATION_UPDATE_BLOCK:
		{
			transitionState_(STATE_TYPE_UPDATING_APPLICATION);
			(void)EventQueue_give(*event);
		} break;

		default:
		{
			transitionState_(STATE_TYPE_IDLING);
		} break;
	}
}

static void Idle_exit(void)
{
}
