
/*
 * linker_config.h
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LINKER_CONFIG_H__
#define __LINKER_CONFIG_H__

//////////////////////////
// INCLUDES
//



////////////////////////////
// TYPES AND MACROS
//

/* ----------------------------------------------------------------------------------------------------------------- */
/*                              The RL78 G23, R7F100GxG device memory map configuration                              */
#define RL78G23_CFM_START_ADDRESS				0x00000000	/* The code flash memory start     */
#define RL78G23_CFM_LENGTH						0x00020000	/* The code flash memory length    */
#define RL78G23_DFM_START_ADDRESS				0x000F1000	/* The data flash memory start     */
#define RL78G23_DFM_LENGTH						0x00002000	/* The data flash memory length    */
#define RL78G23_MAM_START_ADDRESS				0x000F3000	/* The mirror flash memory start   */
#define RL78G23_MAM_LENGTH						0x00008F00	/* The mirror flash memory length  */
#define RL78G23_RAM_START_ADDRESS				0x000FBF00	/* The random access memory start  */
#define RL78G23_RAM_LENGTH						0x00004000	/* The random access memory length */

#define RL78G23_UNIQUE_ID_START_ADDRESS			0x000EFFC0
#define RL78G23_UNIQUE_ID_LENGTH				16

#define RL78G23_PRODUCT_START_ADDRESS			0x000EFFD5
#define RL78G23_PRODUCT_LENGTH					9

#define RL78G23_CFM_BLOCK_CAPACITY				(0xFFFFFFFF - 0xFFFFF7FF)  // 2048
#define RL78G23_DFM_BLOCK_CAPACITY				(0xFFFFFFFF - 0xFFFFFEFF)  // 256

#define RL78G23_CFM_BLOCKS_COUNT				RL78G23_CFM_LENGTH / RL78G23_CFM_BLOCK_CAPACITY
#define RL78G23_DFM_BLOCKS_COUNT				RL78G23_DFM_LENGTH / RL78G23_DFM_BLOCK_CAPACITY
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                           Targets memory configurations                                           */
#define MASTER_BOOT_CFM_START_BLOCK_INDEX		0
#define MASTER_BOOT_CFM_BLOCKS_COUNT			2
#define MASTER_BOOT_CFM_START_ADDRESS			RL78G23_CFM_START_ADDRESS + \
												(RL78G23_CFM_BLOCK_CAPACITY * MASTER_BOOT_CFM_START_BLOCK_INDEX)
#define MASTER_BOOT_CFM_LENGTH					(RL78G23_CFM_BLOCK_CAPACITY * MASTER_BOOT_CFM_BLOCKS_COUNT) - \
												(RL78G23_CFM_BLOCK_CAPACITY * MASTER_BOOT_CFM_START_BLOCK_INDEX)
#define MASTER_BOOT_CFG_BLOCK_INDEX				0
#define MASTER_BOOT_CFG_START_ADDRESS			RL78G23_DFM_START_ADDRESS + \
												(RL78G23_DFM_BLOCK_CAPACITY * MASTER_BOOT_CFG_BLOCK_INDEX)


#define BOOT_LOADER_CFM_START_BLOCK_INDEX		2
#define BOOT_LOADER_CFM_BLOCKS_COUNT			15
#define BOOT_LOADER_CFM_START_ADDRESS			RL78G23_CFM_START_ADDRESS + \
												(RL78G23_CFM_BLOCK_CAPACITY * BOOT_LOADER_CFM_START_BLOCK_INDEX)
#define BOOT_LOADER_CFM_LENGTH					(RL78G23_CFM_BLOCK_CAPACITY * BOOT_LOADER_CFM_BLOCKS_COUNT) - \
												(RL78G23_CFM_BLOCK_CAPACITY * BOOT_LOADER_CFM_START_BLOCK_INDEX)
#define BOOT_LOADER_CFG_BLOCK_INDEX				1
#define BOOT_LOADER_CFG_START_ADDRESS			RL78G23_DFM_START_ADDRESS + \
												(RL78G23_DFM_BLOCK_CAPACITY * BOOT_LOADER_CFG_BLOCK_INDEX)


#define APPLICATION_CFM_START_BLOCK_INDEX		18
#define APPLICATION_CFM_BLOCKS_COUNT			48
#define APPLICATION_CFM_START_ADDRESS			RL78G23_CFM_START_ADDRESS + \
												(RL78G23_CFM_BLOCK_CAPACITY * APPLICATION_CFM_START_BLOCK_INDEX)
#define APPLICATION_CFM_LENGTH					(RL78G23_CFM_BLOCK_CAPACITY * APPLICATION_CFM_BLOCKS_COUNT) - \
												(RL78G23_CFM_BLOCK_CAPACITY * APPLICATION_CFM_START_BLOCK_INDEX)
#define APPLICATION_CFG_BLOCK_INDEX				2
#define APPLICATION_CFG_START_ADDRESS			RL78G23_DFM_START_ADDRESS + \
												(RL78G23_DFM_BLOCK_CAPACITY * APPLICATION_CFG_BLOCK_INDEX)
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                                      Targets                                                      */
#define TARGET_MASTER_BOOT 0
#define TARGET_BOOT_LOADER 1
#define TARGET_APPLICATION 2

// Select the target here:
#define TARGET TARGET_BOOT_LOADER

#if TARGET == TARGET_MASTER_BOOT
#	define TARGET_CFM_START_ADDRESS				MASTER_BOOT_CFM_START_ADDRESS
#	define TARGET_CFM_LENGTH					MASTER_BOOT_CFM_LENGTH
#elif TARGET == TARGET_BOOT_LOADER
#	define TARGET_CFM_START_ADDRESS				BOOT_LOADER_CFM_START_ADDRESS
#	define TARGET_CFM_LENGTH					BOOT_LOADER_CFM_LENGTH
#elif TARGET == TARGET_APPLICATION
#	define TARGET_CFM_START_ADDRESS				APPLICATION_CFM_START_ADDRESS
#	define TARGET_CFM_LENGTH					APPLICATION_CFM_LENGTH
#else
#	error "[fatal error]: invalid target was selected!"
#endif
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                              Master boot cfg section                                              */
#define BOOT_LOADER_VALUE_PRIORITY				1
#define APPLICATION_VALUE_PRIORITY				2

#define MASTER_BOOT_VALUE_BOOT_PRIORITY			BOOT_LOADER_VALUE_PRIORITY
#define MASTER_BOOT_ADDRESS_BOOT_PRIORITY		MASTER_BOOT_CFG_START
#define MASTER_BOOT_LENGTH_BOOT_PRIORITY		1

#define MASTER_BOOT_VALUE_VERSION				0xff00ff00ff00ff
#define MASTER_BOOT_ADDRESS_VERSION				MASTER_BOOT_ADDRESS_BOOT_PRIORITY + MASTER_BOOT_LENGTH_BOOT_PRIORITY
#define MASTER_BOOT_LENGTH_VERSION				8

#define MASTER_BOOT_VALUE_CM_SERIAL_NUMBER		0xaa00aa
#define MASTER_BOOT_ADDRESS_CM_SERIAL_NUMBER	MASTER_BOOT_ADDRESS_VERSION + MASTER_BOOT_LENGTH_VERSION
#define MASTER_BOOT_LENGTH_CM_SERIAL_NUMBER		4

#define MASTER_BOOT_VALUE_BINSIZE				0x69696969
#define MASTER_BOOT_ADDRESS_BINSIZE				MASTER_BOOT_ADDRESS_CM_SERIAL_NUMBER + MASTER_BOOT_LENGTH_CM_SERIAL_NUMBER
#define MASTER_BOOT_LENGTH_BINSIZE				4

#define MASTER_BOOT_VALUE_CRC					0xa5a5
#define MASTER_BOOT_ADDRESS_CRC					MASTER_BOOT_ADDRESS_BINSIZE + MASTER_BOOT_LENGTH_BINSIZE
#define MASTER_BOOT_LENGTH_CRC					2

#define MASTER_BOOT_CFG_LENGTH					MASTER_BOOT_LENGTH_BOOT_PRIORITY    + \
												MASTER_BOOT_LENGTH_VERSION          + \
												MASTER_BOOT_LENGTH_CM_SERIAL_NUMBER + \
												MASTER_BOOT_LENGTH_BINSIZE          + \
												MASTER_BOOT_LENGTH_CRC
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                              Boot loader cfg section                                              */
#define BOOT_LOADER_VALUE_VERSION				0xc5c5c5c5c5c5c5c5
#define BOOT_LOADER_ADDRESS_VERSION				BOOT_LOADER_CFG_START
#define BOOT_LOADER_LENGTH_VERSION				8

#define BOOT_LOADER_VALUE_BINSIZE				0xfafafafa
#define BOOT_LOADER_ADDRESS_BINSIZE				BOOT_LOADER_ADDRESS_VERSION + BOOT_LOADER_LENGTH_VERSION
#define BOOT_LOADER_LENGTH_BINSIZE				4

#define BOOT_LOADER_VALUE_CRC					0xb3b3
#define BOOT_LOADER_ADDRESS_CRC					BOOT_LOADER_ADDRESS_BINSIZE + BOOT_LOADER_LENGTH_BINSIZE
#define BOOT_LOADER_LENGTH_CRC					2

#define BOOT_LOADER_CFG_LENGTH					BOOT_LOADER_LENGTH_VERSION + \
												BOOT_LOADER_LENGTH_BINSIZE + \
												BOOT_LOADER_LENGTH_CRC
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                              Application cfg section                                              */
#define APPLICATION_VALUE_VERSION				0x4646464646464646
#define APPLICATION_ADDRESS_VERSION				APPLICATION_CFG_START
#define APPLICATION_LENGTH_VERSION				8

#define APPLICATION_VALUE_BINSIZE				0xe7e7e7e7
#define APPLICATION_ADDRESS_BINSIZE				APPLICATION_ADDRESS_VERSION + APPLICATION_LENGTH_VERSION
#define APPLICATION_LENGTH_BINSIZE				4

#define APPLICATION_VALUE_CRC					0x7d7d
#define APPLICATION_ADDRESS_CRC					APPLICATION_ADDRESS_BINSIZE + APPLICATION_LENGTH_BINSIZE
#define APPLICATION_LENGTH_CRC					2

#define APPLICATION_CFG_LENGTH					APPLICATION_LENGTH_VERSION + \
												APPLICATION_LENGTH_BINSIZE + \
												APPLICATION_LENGTH_CRC
/* ----------------------------------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------- */
/*                                                   Safety checks                                                   */
#if (BOOT_LOADER_VALUE_PRIORITY <= 0) || (BOOT_LOADER_VALUE_PRIORITY >= 255)
#	error "[fatal error]: boot loader's priority must be in the range of [1; 254]!"
#endif

#if (APPLICATION_VALUE_PRIORITY <= 0) || (APPLICATION_VALUE_PRIORITY >= 255)
#	error "[fatal error]: application's priority must be in the range of [1; 254]!"
#endif

#if BOOT_LOADER_VALUE_PRIORITY == APPLICATION_VALUE_PRIORITY
#	error "[fatal error]: boot loader's and application's priorities must not match!"
#endif

#if (MASTER_BOOT_TARGET_CFM_START_ADDRESS< RL78G23_CFM_START) || \
	((MASTER_BOOT_TARGET_CFM_START_ADDRESS+ MASTER_BOOT_TARGET_CFM_LENGTH - 1) >= BOOT_LOADER_CFM_START)
#	error "[fatal error]: master boot's CFM is in an invalid region!"
#endif

#if (BOOT_LOADER_TARGET_CFM_START_ADDRESS< (MASTER_BOOT_TARGET_CFM_START_ADDRESS+ MASTER_BOOT_TARGET_CFM_LENGTH)) || \
	((BOOT_LOADER_TARGET_CFM_START_ADDRESS+ BOOT_LOADER_TARGET_CFM_LENGTH - 1) >= APPLICATION_CFM_START)
#	error "[fatal error]: bootloader's CFM is in an invalid region!"
#endif

#if (APPLICATION_TARGET_CFM_START_ADDRESS < (BOOT_LOADER_TARGET_CFM_START_ADDRESS + BOOT_LOADER_TARGET_CFM_LENGTH)) || \
	((APPLICATION_TARGET_CFM_START_ADDRESS + APPLICATION_TARGET_CFM_LENGTH - 1) >= (RL78G23_CFM_START_ADDRESS + RL78G23_CFM_LENGTH))
#	error "[fatal error]: application's CFM is in an invalid region!"
#endif

#if MASTER_BOOT_CFG_BLOCK_INDEX == BOOT_LOADER_CFG_BLOCK_INDEX
#	error "[fatal error]: master boot's cfg block index == boot loader's cfg block index!"
#endif

#if BOOT_LOADER_CFG_BLOCK_INDEX == APPLICATION_CFG_BLOCK_INDEX
#	error "[fatal error]: boot loader's cfg block index == application's cfg block index!"
#endif

#if MASTER_BOOT_CFG_LENGTH >= RL78G23_DFM_BLOCK_CAPACITY
#	error "[fatal error]: master boot's CFG length is larger than the max length of the DF block"
#endif

#if BOOT_LOADER_CFG_LENGTH >= RL78G23_DFM_BLOCK_CAPACITY
#	error "[fatal error]: boot loader's CFG length is larger than the max length of the DF block"
#endif

#if APPLICATION_CFG_LENGTH >= RL78G23_DFM_BLOCK_CAPACITY
#	error "[fatal error]: application's CFG length is larger than the max length of the DF block"
#endif

#if (MASTER_BOOT_CFG_START_ADDRESS < RL78G23_DFM_START_ADDRESS) || \
	((MASTER_BOOT_CFG_START_ADDRESS + RL78G23_DFM_BLOCK_CAPACITY - 1) >= BOOT_LOADER_CFG_START_ADDRESS)
#	error "[fatal error]: master boot's CFG is in an invalid region!"
#endif

#if (BOOT_LOADER_CFG_START_ADDRESS < (MASTER_BOOT_CFG_START_ADDRESS + MASTER_BOOT_TARGET_CFG_LENGTH)) || \
	((BOOT_LOADER_CFG_START_ADDRESS + RL78G23_DFM_BLOCK_CAPACITY - 1) >= APPLICATION_CFG_START_ADDRESS)
#	error "[fatal error]: bootloader's CFG is in an invalid region!"
#endif

#if (APPLICATION_CFG_START_ADDRESS < (BOOT_LOADER_CFG_START_ADDRESS + RL78G23_DFM_BLOCK_CAPACITY)) || \
	((APPLICATION_CFG_START_ADDRESS + RL78G23_DFM_BLOCK_CAPACITY - 1) >= (RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH))
#	error "[fatal error]: application's CFG is in an invalid region!"
#endif

#if MASTER_BOOT_CFG_LENGTH > 255
#	error "[fatal error]: master boot's CFG is overflowing!"
#endif

#if BOOT_LOADER_CFG_LENGTH > 255
#	error "[fatal error]: boot loader's CFG is overflowing"
#endif

#if APPLICATION_CFG_LENGTH > 255
#	error "[fatal error]: application's CFG is overflowing"
#endif
/* ----------------------------------------------------------------------------------------------------------------- */



//////////////////////////
// PUBLIC INTERFACE
//
#ifndef LINKER_CONFIG_DISABLE_C_API
#include <stdint.h>

typedef struct __attribute__((packed))
{
	// NOTE: The order of these fields is critical!
	uint8_t  bootPriority;
	uint64_t version;
	uint32_t serialNumber;
	uint32_t binsize;
	uint16_t crc;
} MasterBootConfig_t;

typedef struct __attribute__((packed))
{
	// NOTE: The order of these fields is critical!
	uint64_t version;
	uint32_t binsize;
	uint16_t crc;
} BootLoaderConfig_t;

typedef struct __attribute__((packed))
{
	// NOTE: The order of these fields is critical!
	uint64_t version;
	uint32_t binsize;
	uint16_t crc;
} ApplicationConfig_t;
#endif /* LINKER_CONFIG_ENABLE_C_API */

#endif /* __LINKER_CONFIG_H__ */
