
/*
 * l3_network.h
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __L3__L3_NETWORK_H__
#define __L3__L3_NETWORK_H__

//////////////////////////
// INCLUDES
//
#include "l2/l2_network.h"

#include "l3/l3_packet.h"

#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//

enum
{
	L3_TX_STATUS_OK,
	L3_TX_STATUS_SEND_FAILED,
	L3_TX_STATUS_RESEND_FAILED,
	L3_TX_STATUSES_COUNT
};

typedef uint8_t L3_TxStatus_t;

typedef struct
{
	void(*function)(const L3_TxStatus_t, L3_PacketHandle_t, void*);
	void* arguments;
} L3_NetworkTxCallback_t;

#define L3_NETWORK_TX_CALLBACK(_function, _arguments) \
	(L3_NetworkTxCallback_t) { .function = _function, .arguments = _arguments }



//////////////////////////
// PUBLIC INTERFACE
//
void L3_Network_create(
	L2_NetworkHandle_t bottomL2Network,
	L2_NetworkHandle_t topL2Network);

void L3_Network_setCellId(const L3_CellId_t cellId);

bool L3_Network_run(void);

bool L3_Network_receivePacket(L3_PacketHandle_t l3Packet);

bool L3_Network_sendPacket(
	L3_PacketHandle_t l3Packet,
	const L3_NetworkTxCallback_t callback);

#endif /* __L3__L3_NETWORK_H__ */
