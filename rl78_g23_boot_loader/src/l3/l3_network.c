
/*
 * l3_network.c
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "l3/l3_network.h"

#include "debug/asserts.h"

#include "l2/l2_serial.h"

#include "l3/l3_packet.h"
#include "l3/l3_packet_ext.h"

#include <stdint.h>



////////////////////////////
// PRIVATE TYPES AND MACROS
//
enum
{
	L3_NETWORK_RX_STATE_IDLING,
	L3_NETWORK_RX_STATE_RECEIVING_BOTTOM,
	L3_NETWORK_RX_STATE_RECEIVING_TOP,
	L3_NETWORK_RX_STATE_PROCESSING,
	L3_NETWORK_RX_STATE_HANDLING_NON_RAW_PACKET,
	L3_NETWORK_RX_STATE_RECEIVED_PACKET,
	L3_NETWORK_RX_STATES_COUNT
};

typedef uint8_t L3_NetworkRxState_t;

enum
{
	L3_NETWORK_TX_STATE_IDLING,
	L3_NETWORK_TX_STATE_SEND_PENDING,
	L3_NETWORK_TX_STATE_RESEND_PENDING,
	L3_NETWORK_TX_STATES_COUNT
};

typedef uint8_t L3_NetworkTxState_t;

typedef struct
{
	// Assuming only two physical serial ports are in use for communication: /|
	L2_NetworkHandle_t bottomL2Network;	// <--------------------------------+ |
	L2_NetworkHandle_t topL2Network;	// <----------------------------------+
	L3_Packet_t currentRxPacket;
	L3_NetworkRxState_t rxState;
	L3_Packet_t currentTxPacket;
	L3_NetworkTxState_t txState;
	L3_NetworkTxCallback_t txCallback;
	L3_CellId_t cellId;
} L3_Network_t;



////////////////////////////
// PRIVATE ATTRIBUTES
//
static L3_Network_t l3Network_;

// WARNING: Must NOT include the L3_PACKET_TYPE_RAW type!
static L3_PacketHandlerFunction_t nonRawPacketHandlers_[L3_PACKET_TYPES_COUNT] =
{
	[L3_PACKET_TYPE_ENUMERATE_CELLS] 			= L3_EnumerateCellsPacket_handle,
	[L3_PACKET_TYPE_GET_SERIAL_NUMBER] 			= L3_GetSerialNumberPacket_handle,
	[L3_PACKET_TYPE_GET_SW_VERSION] 			= L3_GetSoftwareVersionPacket_handle,
	[L3_PACKET_TYPE_GET_HW_STATS] 				= NULL,
	[L3_PACKET_TYPE_CHANGE_RUNNING_PARTITION] 	= NULL,
};



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//

// L3 packet reception functions
static bool receivePossibleL3Packet_(L2_NetworkHandle_t l2Network);
static bool isCurrentL3PacketValid_(void);
static bool isCurrentL3PacketRaw_(void);
static void handleCurrentNonRawL3Packet_(void);

// L3 packet transmission functions
static void handleL2PacketSent_(
	L2_PacketHandle_t l2Packet,
	bool success);



////////////////////////////
// IMPLEMENTATION
//
void L3_Network_create(
	L2_NetworkHandle_t bottomL2Network,
	L2_NetworkHandle_t topL2Network)
{
	DEBUG_ASSERT(bottomL2Network);
	DEBUG_ASSERT(topL2Network);

	l3Network_.bottomL2Network = bottomL2Network;
	l3Network_.topL2Network = topL2Network;

	l3Network_.currentRxPacket = (L3_Packet_t) {0};
	l3Network_.rxState = L3_NETWORK_RX_STATE_IDLING;

	l3Network_.currentTxPacket = (L3_Packet_t) {0};
	l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
	l3Network_.txCallback = (L3_NetworkTxCallback_t) {0};

	// TODO: revert:
	l3Network_.cellId = 0; // L3_CELL_ID_UNDEFINED;
}

void L3_Network_setCellId(const L3_CellId_t cellId)
{
	l3Network_.cellId = cellId;
}

bool L3_Network_run(void)
{
	switch (l3Network_.rxState)
	{
		case L3_NETWORK_RX_STATE_IDLING:
		{
			l3Network_.rxState = L3_NETWORK_RX_STATE_RECEIVING_BOTTOM;
		} break;

		case L3_NETWORK_RX_STATE_RECEIVING_BOTTOM:
		{
			if (receivePossibleL3Packet_(l3Network_.bottomL2Network))
			{
				l3Network_.rxState = L3_NETWORK_RX_STATE_PROCESSING;
			}
			else
			{
				l3Network_.rxState = L3_NETWORK_RX_STATE_RECEIVING_TOP;
			}
		} break;

		case L3_NETWORK_RX_STATE_RECEIVING_TOP:
		{
			if (receivePossibleL3Packet_(l3Network_.topL2Network))
			{
				l3Network_.rxState = L3_NETWORK_RX_STATE_PROCESSING;
			}
			else
			{
				l3Network_.rxState = L3_NETWORK_RX_STATE_RECEIVING_BOTTOM;
			}
		} break;

		case L3_NETWORK_RX_STATE_PROCESSING:
		{
			if (isCurrentL3PacketValid_())
			{
				if (isCurrentL3PacketRaw_())
				{
					l3Network_.rxState = L3_NETWORK_RX_STATE_RECEIVED_PACKET;
				}
				else
				{
					l3Network_.rxState = L3_NETWORK_RX_STATE_HANDLING_NON_RAW_PACKET;
				}
			}
			else
			{
				l3Network_.rxState = L3_NETWORK_RX_STATE_IDLING;
			}
		} break;

		case L3_NETWORK_RX_STATE_HANDLING_NON_RAW_PACKET:
		{
			handleCurrentNonRawL3Packet_();
			l3Network_.rxState = L3_NETWORK_RX_STATE_RECEIVED_PACKET;
		} break;

		case L3_NETWORK_RX_STATE_RECEIVED_PACKET:
		{
			// NOTE: Do nothing here - to exit this rxState, the RAW packet must be
			//       received using L3_Network_receivePacket() function.
		} break;

		// Should never ever reach this block
		default:
		{
			DEBUG_ASSERT(0);
		} break;
	}

	return l3Network_.rxState == L3_NETWORK_RX_STATE_RECEIVED_PACKET;
}

bool L3_Network_receivePacket(L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);

	if (l3Network_.rxState != L3_NETWORK_RX_STATE_RECEIVED_PACKET)
	{
		return false;
	}

	l3Network_.rxState = L3_NETWORK_RX_STATE_IDLING;
	*l3Packet = l3Network_.currentRxPacket;
	return true;
}

bool L3_Network_sendPacket(
	L3_PacketHandle_t l3Packet,
	const L3_NetworkTxCallback_t callback)
{
	DEBUG_ASSERT(l3Packet);

	if (l3Network_.txState == L3_NETWORK_TX_STATE_SEND_PENDING
	 || l3Network_.txState == L3_NETWORK_TX_STATE_RESEND_PENDING)
	{
		return false;
	}

	l3Network_.currentTxPacket = *l3Packet;
	l3Network_.txCallback = callback;

	const bool isFlipped =
		l3Network_.currentTxPacket.header.status == L3_PACKET_STATUS_FLIPPED;
	l3Network_.txState = isFlipped ?
		L3_NETWORK_TX_STATE_RESEND_PENDING :
		L3_NETWORK_TX_STATE_SEND_PENDING;

	L2_NetworkHandle_t outgoingL2Network;
	const L3_PacketDirection_t direction =
		l3Network_.currentTxPacket.header.direction;

	if (l3Network_.currentTxPacket.header.status == L3_PACKET_STATUS_FLIPPED)
	{
		if (direction == L3_PACKET_DIRECTION_FROM_BOTTOM_TO_TOP)
		{
			outgoingL2Network = l3Network_.bottomL2Network;
		}
		else // L3_PACKET_DIRECTION_FROM_TOP_TO_BOTTOM
		{
			outgoingL2Network = l3Network_.topL2Network;
		}
	}
	else
	{
		if (direction == L3_PACKET_DIRECTION_FROM_BOTTOM_TO_TOP)
		{
			outgoingL2Network = l3Network_.topL2Network;
		}
		else // L3_PACKET_DIRECTION_FROM_TOP_TO_BOTTOM
		{
			outgoingL2Network = l3Network_.bottomL2Network;
		}
	}

	const L3_PacketLength_t length =
		L3_Packet_getPacketLength(&l3Network_.currentTxPacket);

	if (L2_Network_sendPacket(
			outgoingL2Network,
			(const uint8_t*)&l3Network_.currentTxPacket,
			length,
			handleL2PacketSent_) == SEND_OK)
	{
		return true;
	}
	else
	{
		l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
		return false;
	}
}

static bool receivePossibleL3Packet_(L2_NetworkHandle_t l2Network)
{
	DEBUG_ASSERT(l2Network);

	if (L2_Network_run(l2Network))
	{
		L2_PacketHandle_t l2Packet = NULL;

		// L2 receivePacket only returns L2 RAW types
		if (L2_Network_receivePacket(l2Network, &l2Packet) == RECV_OK)
		{
			l3Network_.currentRxPacket = *(L3_PacketHandle_t)l2Packet->data;
			return true;
		}
	}

	return false;
}

static bool isCurrentL3PacketValid_(void)
{
	const L3_PacketType_t type = l3Network_.currentRxPacket.header.packetType;
	return type >= 0 && type < L3_PACKET_TYPES_COUNT;
}

static bool isCurrentL3PacketRaw_(void)
{
	return l3Network_.currentRxPacket.header.packetType == L3_PACKET_TYPE_RAW;
}

static void handleCurrentNonRawL3Packet_(void)
{
	L3_PacketHandle_t l3Packet = &l3Network_.currentRxPacket;

	if (L3_Packet_isRecipientOfThePacket(l3Network_.cellId, l3Packet))
	{
		L3_PacketHandlerFunction_t packetHandler =
			nonRawPacketHandlers_[l3Packet->header.packetType];
		DEBUG_ASSERT(packetHandler);
		packetHandler(l3Packet);
	}

	if (L3_Packet_isFirstTripLastRecipient(l3Network_.cellId, l3Packet))
	{
		l3Packet->header.status = L3_PACKET_STATUS_FLIPPED;
	}
}

static void handleL2PacketSent_(
	L2_PacketHandle_t l2Packet,
	bool success)
{
	if (success)
	{
		if (l3Network_.txCallback.function)
		{
			l3Network_.txCallback.function(
				L3_TX_STATUS_OK,
				&l3Network_.currentTxPacket,
				l3Network_.txCallback.arguments
			);
		}

		l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
	}
	else
	{
		// NOTE: This must be reviewed and fixed. The l2Packet that is the first argument in this
		//       function is never passed as a full L2_Packet_t from the higher order function. In
		//       reality, the l2Packet parameter is the payload/data handle of L2 packet. In other
		//       words, it is anything but full L2 packet!
		// 
		// TODO: Review this with Paulius and fix it:
		// 
		l3Network_.currentTxPacket = *(L3_PacketHandle_t)l2Packet;
		L3_TxStatus_t status = L3_TX_STATUS_OK;

		if (l3Network_.txState == L3_NETWORK_TX_STATE_RESEND_PENDING)
		{
			l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
			status = L3_TX_STATUS_RESEND_FAILED;
		}
		else if (l3Network_.txState == L3_NETWORK_TX_STATE_SEND_PENDING)
		{
			l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
			l3Network_.currentTxPacket.header.status = L3_PACKET_STATUS_FLIPPED;
			status = L3_TX_STATUS_SEND_FAILED;
		}
		else
		{
			l3Network_.txState = L3_NETWORK_TX_STATE_IDLING;
			status = L3_TX_STATUS_RESEND_FAILED;
		}

		if (l3Network_.txCallback.function)
		{
			l3Network_.txCallback.function(
				status,
				&l3Network_.currentTxPacket,
				l3Network_.txCallback.arguments
			);
		}
	}
}
