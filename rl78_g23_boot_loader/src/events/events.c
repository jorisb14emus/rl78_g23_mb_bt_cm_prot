
/*
 * events.c
 *
 * Created on: 2 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "events/events.h"

#include "debug/asserts.h"

#include "ring_buffer/ring_buffer.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//
#define EVENT_RING_BUFFER_CAPACITY 16

DEFINE_RING_BUFFER(EventRingBuffer, Event_t, EVENT_RING_BUFFER_CAPACITY);



////////////////////////////
// PRIVATE ATTRIBUTES
//
static EventRingBuffer_t eventBuffer_ = (EventRingBuffer_t)
{
	.head = 0,
	.tail = 0
};



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
bool EventQueue_give(const Event_t event)
{
	bool result = false;
	DI();
	result = EventRingBuffer_give(&eventBuffer_, event);
	EI();
	return result;
}

Event_t EventQueue_take(void)
{
	Event_t result;
	DI();
	if (!EventRingBuffer_take(&eventBuffer_, &result))
	{
		result.type = EVENT_TYPE_NONE;
	}
	EI();
	return result;
}
