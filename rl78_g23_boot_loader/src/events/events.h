
/*
 * events.h
 *
 * Created on: 12 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __EVENTS__EVENTS_H__
#define __EVENTS__EVENTS_H__

//////////////////////////
// INCLUDES
//
#include "l3/l3_packet.h"

#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
enum
{
	EVENT_TYPE_NONE,
	EVENT_TYPE_DISPATCH_PACKET,
	EVENT_TYPE_RECEIVED_APPLICATION_UPDATE_BLOCK,
	EVENT_TYPES_COUNT
};

typedef uint8_t EventType_t;

typedef struct
{
	EventType_t type;

	union
	{
		L3_Packet_t l3Packet;
	};
} Event_t;

typedef Event_t* EventHandle_t;



//////////////////////////
// PUBLIC INTERFACE
//
bool EventQueue_give(const Event_t event);

Event_t EventQueue_take(void);

#endif /* __EVENTS__EVENTS_H__ */
