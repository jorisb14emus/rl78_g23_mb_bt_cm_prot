
/*
 * ll_nvm.c
 *
 * Created on: 25 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "ll_nvm/ll_nvm.h"

#include "linker_config.h"

#include "debug/asserts.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"

#include <memory.h>



////////////////////////////
// PRIVATE TYPES AND MACROS
//
#define LL_NVM_REGISTER_U01_DFLCTL_DFLEN								DFLEN
#define LL_NVM_REGISTER_U08_FSSET										FSSET
#define LL_NVM_REGISTER_U08_FSSE										FSSE
#define LL_NVM_REGISTER_U08_PFCMD										PFCMD
#define LL_NVM_REGISTER_U08_FLPMC										FLPMC
#define LL_NVM_REGISTER_U08_FLARS										FLARS
#define LL_NVM_REGISTER_U16_FLAPL										FLAPL
#define LL_NVM_REGISTER_U08_FLAPH										FLAPH
#define LL_NVM_REGISTER_U08_FSSQ										FSSQ
#define LL_NVM_REGISTER_U16_FLSEDL										FLSEDL
#define LL_NVM_REGISTER_U08_FLSEDH										FLSEDH
#define LL_NVM_REGISTER_U08_FLRST										FLRST
#define LL_NVM_REGISTER_U08_FSASTL										FSASTL
#define LL_NVM_REGISTER_U08_FSASTH										FSASTH
#define LL_NVM_REGISTER_U16_FLWL										FLWL
#define LL_NVM_REGISTER_U16_FLWH										FLWH

#define LL_NVM_VALUE_U01_DFLEN_DATA_FLASH_ACCESS_DISABLE				0u
#define LL_NVM_VALUE_U01_DFLEN_DATA_FLASH_ACCESS_ENABLE					1u
#define LL_NVM_VALUE_U08_FLARS_USER_AREA								0x00u
#define LL_NVM_VALUE_U08_FSSQ_WRITE										0x81u
#define LL_NVM_VALUE_U08_FSSQ_BLANKCHECK_CF								0x83u
#define LL_NVM_VALUE_U08_FSSQ_BLANKCHECK_DF								0x8Bu
#define LL_NVM_VALUE_U08_FSSQ_ERASE										0x84u
#define LL_NVM_VALUE_U08_FSSQ_CLEAR										0x00u
#define LL_NVM_VALUE_U08_PFCMD_SPECIFIC_SEQUENCE_WRITE					0xA5u
#define LL_NVM_VALUE_U08_FLPMC_MODE_UNPROGRAMMABLE_FWEDIS_ENABLE		0x00u
#define LL_NVM_VALUE_U08_FLPMC_MODE_UNPROGRAMMABLE_FWEDIS_DISABLE		0x08u
#define LL_NVM_VALUE_U08_FLPMC_MODE_CODE_FLASH_PROGRAMMING				0x02u
#define LL_NVM_VALUE_U08_FLPMC_MODE_DATA_FLASH_PROGRAMMING				0x10u
#define LL_NVM_VALUE_U08_FLRST_ON										0x01u
#define LL_NVM_VALUE_U08_FLRST_OFF										0x00u

#define LL_NVM_VALUE_U08_SHIFT_8BIT										8u
#define LL_NVM_VALUE_U08_SHIFT_16BIT									16u

#define LL_NVM_VALUE_U08_SET_FWEDIS_FLAG_ON								0x55u
#define LL_NVM_VALUE_U08_SET_FWEDIS_FLAG_OFF							0x00u
#define LL_NVM_VALUE_U16_CODE_FLASH_BLOCK_ADDR_END						0x07FCu
#define LL_NVM_VALUE_U16_DATA_FLASH_BLOCK_ADDR_END						0x00FFu

#define LL_NVM_VALUE_U08_MASK1_8BIT										0xFFu
#define LL_NVM_VALUE_U16_MASK1_16BIT									0xFFFFu

#define LL_NVM_VALUE_U08_MASK1_FLPMC_FWEDIS								0x08u
#define LL_NVM_VALUE_U08_MASK0_FLPMC_FWEDIS								0xF7u
#define LL_NVM_VALUE_U08_MASK1_FSASTH_SQEND								0x40u
#define LL_NVM_VALUE_U08_MASK1_FSASTH_ESQEND							0x80u
#define LL_NVM_VALUE_U08_MASK1_FSSET_TMSPMD_AND_TMBTSEL					0xC0u
#define LL_NVM_VALUE_U08_MASK0_FSSET_TMSPMD_AND_TMBTSEL					0x3Fu
#define LL_NVM_VALUE_U08_MASK1_FSSET_TMSPMD								0x80u
#define LL_NVM_VALUE_U08_MASK1_FSASTL_ERROR_FLAG						0x3Fu
#define LL_NVM_VALUE_U16_MASK1_FLFSW_BLOCK_NUMBER						0x01FFu
#define LL_NVM_VALUE_U16_MASK1_FLFSWE_FSWC								0x8000u
#define LL_NVM_VALUE_U16_MASK1_FLFSWS_FSPR								0x8000u

#define LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_FLASH_MEMORY_SEQUENCER_ERROR	0x10u
#define LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_BLANK_CHECKING_ERROR			0x08u
#define LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_BLOCK_ERASURE_ERROR			0x01u
#define LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_DATA_WRITING_ERROR			0x02u

#define LL_NVM_VALUE_U01_MASK0_1BIT										0u
#define LL_NVM_VALUE_U01_MASK1_1BIT										1u
#define LL_NVM_VALUE_U08_MASK0_8BIT										0x00u
#define LL_NVM_VALUE_U08_MASK1_8BIT										0xFFu

#define LL_NVM_CODE_FLASH_BLOCK_SIZE									2048
#define LL_NVM_DATA_FLASH_BLOCK_SIZE									256



////////////////////////////
// PRIVATE ATTRIBUTES
//
/* Driver initialization flag */
static bool isInitialized_ = false;
/* Flag whether the interrupt vector is changed */
static uint8_t changeInterruptVectorFlag_ = 0;
/* CPU Frequency configuration for r_rfd_wait_count */
static uint8_t cpuFrequencyMHz_ = 0;



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
static void delayMicroseconds_(const uint16_t microSecs);

static bool checkFlashMode_(const LL_NvmFlashMode_t flashMode);

static bool isFirstPhaseCompleted_(void);
static bool isSecondPhaseCompleted_(void);

static void getSequencerStatus_(uint8_t* const status);
static void clearSequencerRegister_(void);



////////////////////////////
// IMPLEMENTATION
//
void LL_Nvm_init(const LL_NvmFrequency_t frequencyMHz)
{
	// Checking if high speed oscillator is enabled:
	DEBUG_ASSERT(LL_NVM_VALUE_U01_MASK0_1BIT == HIOSTOP);
	DEBUG_ASSERT(frequencyMHz >= 0 && frequencyMHz < LL_NVM_FREQUENCIES_COUNT);
	cpuFrequencyMHz_ = (uint8_t)frequencyMHz;
	changeInterruptVectorFlag_ = 0;
	isInitialized_ = true;
}

bool LL_Nvm_isInitialized(void)
{
	return isInitialized_;
}

void LL_Nvm_setDataFlashAccessMode(const LL_NvmDfAccessMode_t accessMode)
{
	if (LL_NVM_DF_ACCESS_MODE_ENABLE == accessMode)
	{
		LL_NVM_REGISTER_U01_DFLCTL_DFLEN = LL_NVM_VALUE_U01_DFLEN_DATA_FLASH_ACCESS_ENABLE;
	}
	else
	{
		LL_NVM_REGISTER_U01_DFLCTL_DFLEN = LL_NVM_VALUE_U01_DFLEN_DATA_FLASH_ACCESS_DISABLE;
	}
}

bool LL_Nvm_setFlashMode(const LL_NvmFlashMode_t flashMode)
{
	uint8_t flpmcValue;

	if (LL_NVM_FLASH_MODE_DATA_PROGRAMMING == flashMode)
	{
		flpmcValue = LL_NVM_VALUE_U08_FLPMC_MODE_DATA_FLASH_PROGRAMMING;
	}
	else if (LL_NVM_FLASH_MODE_CODE_PROGRAMMING == flashMode)
	{
		flpmcValue = LL_NVM_VALUE_U08_FLPMC_MODE_CODE_FLASH_PROGRAMMING;
	}
	else
	{
		/* When using LL_NVM_ChangeInterruptVector() */
		if (LL_NVM_VALUE_U08_SET_FWEDIS_FLAG_ON == changeInterruptVectorFlag_)
		{
			/* Non-programmable mode (FWEDIS enable) */
			flpmcValue = LL_NVM_VALUE_U08_FLPMC_MODE_UNPROGRAMMABLE_FWEDIS_ENABLE;
		}
		else /* When not using LL_NVM_ChangeInterruptVector() */
		{
			/* Non-programmable mode (FWEDIS disable) */
			flpmcValue = LL_NVM_VALUE_U08_FLPMC_MODE_UNPROGRAMMABLE_FWEDIS_DISABLE;
		}
	}

	DI();
	LL_NVM_REGISTER_U08_PFCMD = LL_NVM_VALUE_U08_PFCMD_SPECIFIC_SEQUENCE_WRITE;
	LL_NVM_REGISTER_U08_FLPMC = flpmcValue;
	LL_NVM_REGISTER_U08_FLPMC = ~flpmcValue;
	LL_NVM_REGISTER_U08_FLPMC = flpmcValue;
	EI();

	delayMicroseconds_(15u);
	bool result = checkFlashMode_(flashMode);
	uint8_t fssetValue = LL_NVM_REGISTER_U08_FSSET;

	if (result)
	{
		if (LL_NVM_FLASH_MODE_UNPROGRAMMABLE != flashMode)
		{
			LL_NVM_REGISTER_U08_FSSET =
				(fssetValue & LL_NVM_VALUE_U08_MASK1_FSSET_TMSPMD_AND_TMBTSEL) | cpuFrequencyMHz_;
		}
	}

	return result;
}

void LL_Nvm_Df_readBytes(
	const uint16_t address,
	uint8_t* const buffer,
	const uint8_t count)
{
	DEBUG_ASSERT(buffer);
	const uint32_t absoluteAddress = RL78G23_DFM_START_ADDRESS + address;
	DEBUG_ASSERT(absoluteAddress >= RL78G23_DFM_START_ADDRESS &&
		(absoluteAddress + count < RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH));
	(void)memcpy((void*)buffer, (const void*)absoluteAddress, count);
}

void LL_Nvm_Df_eraseBlock(const uint16_t blockIndex)
{
	const uint32_t absoluteAddress = RL78G23_DFM_START_ADDRESS + (blockIndex * LL_NVM_DATA_FLASH_BLOCK_SIZE);
	DEBUG_ASSERT(absoluteAddress >= RL78G23_DFM_START_ADDRESS &&
		(absoluteAddress + LL_NVM_DATA_FLASH_BLOCK_SIZE < RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH));

	const uint16_t lowAddress = absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT;
	const uint8_t highAddress = ((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = lowAddress;
	LL_NVM_REGISTER_U08_FLAPH = highAddress;

	LL_NVM_REGISTER_U16_FLSEDL = lowAddress | LL_NVM_VALUE_U16_DATA_FLASH_BLOCK_ADDR_END;
	LL_NVM_REGISTER_U08_FLSEDH = highAddress;

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_ERASE;
}

void LL_Nvm_Df_blankCheck(const uint16_t blockIndex)
{
	const uint32_t absoluteAddress = RL78G23_DFM_START_ADDRESS + (blockIndex * LL_NVM_DATA_FLASH_BLOCK_SIZE);
	DEBUG_ASSERT(absoluteAddress >= RL78G23_DFM_START_ADDRESS &&
		(absoluteAddress + LL_NVM_DATA_FLASH_BLOCK_SIZE < RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH));

	const uint16_t lowAddress = absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT;
	const uint8_t highAddress = ((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = lowAddress;
	LL_NVM_REGISTER_U08_FLAPH = highAddress;

	LL_NVM_REGISTER_U16_FLSEDL = lowAddress | LL_NVM_VALUE_U16_DATA_FLASH_BLOCK_ADDR_END;
	LL_NVM_REGISTER_U08_FLSEDH = highAddress;

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_BLANKCHECK_DF;
}

void LL_Nvm_Df_writeBytes(
	const uint16_t address,
	const uint8_t bytes[1])
{
	DEBUG_ASSERT(bytes);
	const uint32_t absoluteAddress = RL78G23_DFM_START_ADDRESS + address;
	DEBUG_ASSERT(absoluteAddress >= RL78G23_DFM_START_ADDRESS &&
		(absoluteAddress + 1 < RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH));

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = (uint16_t)(absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT);
	LL_NVM_REGISTER_U08_FLAPH = (uint8_t)((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U16_FLWL = (uint16_t)(*(bytes + 0x00));

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_WRITE;
}

void LL_Nvm_Cf_readBytes(
	const uint16_t address,
	uint8_t* const buffer,
	const uint8_t count)
{
	DEBUG_ASSERT(buffer);
	const uint32_t absoluteAddress = RL78G23_CFM_START_ADDRESS + address;
	DEBUG_ASSERT(absoluteAddress >= RL78G23_CFM_START_ADDRESS &&
		(absoluteAddress + count < RL78G23_CFM_START_ADDRESS + RL78G23_CFM_LENGTH));
	(void)memcpy((void*)buffer, (const void*)absoluteAddress, count);
}

void LL_Nvm_Cf_eraseBlock(const uint16_t blockIndex)
{
	const uint32_t absoluteAddress = RL78G23_CFM_START_ADDRESS + (blockIndex * LL_NVM_CODE_FLASH_BLOCK_SIZE);
	DEBUG_ASSERT(absoluteAddress >= RL78G23_CFM_START_ADDRESS &&
		(absoluteAddress + LL_NVM_CODE_FLASH_BLOCK_SIZE < RL78G23_CFM_START_ADDRESS + RL78G23_CFM_LENGTH));

	const uint16_t lowAddress = absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT;
	const uint8_t highAddress = ((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = lowAddress;
	LL_NVM_REGISTER_U08_FLAPH = highAddress;

	LL_NVM_REGISTER_U16_FLSEDL = lowAddress | LL_NVM_VALUE_U16_CODE_FLASH_BLOCK_ADDR_END;
	LL_NVM_REGISTER_U08_FLSEDH = highAddress;

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_ERASE;
}

void LL_Nvm_Cf_blankCheck(const uint16_t blockIndex)
{
	const uint32_t absoluteAddress = RL78G23_CFM_START_ADDRESS + (blockIndex * LL_NVM_CODE_FLASH_BLOCK_SIZE);
	DEBUG_ASSERT(absoluteAddress >= RL78G23_CFM_START_ADDRESS &&
		(absoluteAddress + LL_NVM_CODE_FLASH_BLOCK_SIZE < RL78G23_CFM_START_ADDRESS + RL78G23_CFM_LENGTH));

	const uint16_t lowAddress = absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT;
	const uint8_t highAddress = ((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = lowAddress;
	LL_NVM_REGISTER_U08_FLAPH = highAddress;

	LL_NVM_REGISTER_U16_FLSEDL = lowAddress | LL_NVM_VALUE_U16_CODE_FLASH_BLOCK_ADDR_END;
	LL_NVM_REGISTER_U08_FLSEDH = highAddress;

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_BLANKCHECK_CF;
}

void LL_Nvm_Cf_writeBytes(
	const uint16_t address,
	const uint8_t bytes[4])
{
	DEBUG_ASSERT(bytes);
	const uint32_t absoluteAddress = RL78G23_DFM_START_ADDRESS + address;
	DEBUG_ASSERT(absoluteAddress >= RL78G23_DFM_START_ADDRESS &&
		(absoluteAddress + 4 < RL78G23_DFM_START_ADDRESS + RL78G23_DFM_LENGTH));

	LL_NVM_REGISTER_U08_FLARS = LL_NVM_VALUE_U08_FLARS_USER_AREA;

	LL_NVM_REGISTER_U16_FLAPL = (uint16_t)(absoluteAddress & LL_NVM_VALUE_U16_MASK1_16BIT);
	LL_NVM_REGISTER_U08_FLAPH = (uint8_t)((absoluteAddress >> LL_NVM_VALUE_U08_SHIFT_16BIT) & LL_NVM_VALUE_U08_MASK1_8BIT);

	LL_NVM_REGISTER_U16_FLWL = ((uint16_t)(*(bytes + 0x00))) 
		| (((uint16_t)(*(bytes + 0x01))) << LL_NVM_VALUE_U08_SHIFT_8BIT);
	LL_NVM_REGISTER_U16_FLWH = ((uint16_t)(*(bytes + 0x02))) 
		| (((uint16_t)(*(bytes + 0x03))) << LL_NVM_VALUE_U08_SHIFT_8BIT);

	LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_WRITE;
}

bool LL_Nvm_await(void)
{
	while (!isFirstPhaseCompleted_())
	{
		NOP();
		/* It is possible to write the program for detecting timeout here as necessity requires */
	}

	while (!isSecondPhaseCompleted_())
	{
		NOP();
		/* It is possible to write the program for detecting timeout here as necessity requires */
	}

	/* Action error check */
	uint8_t statusFlag = 0;
	getSequencerStatus_(&statusFlag);
	bool result = true;

	if (LL_NVM_VALUE_U08_MASK0_8BIT != (statusFlag & LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_FLASH_MEMORY_SEQUENCER_ERROR))
	{
		result = false;
	}
	else if (LL_NVM_VALUE_U08_MASK0_8BIT != (statusFlag & LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_BLOCK_ERASURE_ERROR))
	{
		result = false;
	}
	else if (LL_NVM_VALUE_U08_MASK0_8BIT != (statusFlag & LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_DATA_WRITING_ERROR))
	{
		result = false;
	}
	else if (LL_NVM_VALUE_U08_MASK0_8BIT != (statusFlag & LL_NVM_VALUE_U08_MASK1_FSQ_STATUS_BLANK_CHECKING_ERROR))
	{
		result = false;
	}
	else
	{
		result = true;
	}

	clearSequencerRegister_();
	return result;
}

static void delayMicroseconds_(const uint16_t microSecs)
{
	// Compensating for in-for instructions -> dividing by 3.
	const uint16_t cycles = cpuFrequencyMHz_ * microSecs / 3;
	for (uint16_t cycle = 0; cycle < cycles; ++cycle) NOP();
}

static bool checkFlashMode_(const LL_NvmFlashMode_t flashMode)
{
	bool result = true;
	uint8_t flpmcExpectedValue;
	uint8_t flpmcValue = LL_NVM_REGISTER_U08_FLPMC;

	if (LL_NVM_FLASH_MODE_DATA_PROGRAMMING == flashMode)
	{
		flpmcExpectedValue = LL_NVM_VALUE_U08_FLPMC_MODE_DATA_FLASH_PROGRAMMING;
	}
	else if (LL_NVM_FLASH_MODE_CODE_PROGRAMMING == flashMode)
	{
		flpmcExpectedValue = LL_NVM_VALUE_U08_FLPMC_MODE_CODE_FLASH_PROGRAMMING;
	}
	else
	{
		flpmcValue &= LL_NVM_VALUE_U08_MASK0_FLPMC_FWEDIS;
		flpmcExpectedValue = LL_NVM_VALUE_U08_FLPMC_MODE_UNPROGRAMMABLE_FWEDIS_ENABLE;
	}

	result = (flpmcExpectedValue == flpmcValue);
	return result;
}

static bool isFirstPhaseCompleted_(void)
{
	bool status = true;
	const uint8_t fsasthValue = LL_NVM_REGISTER_U08_FSASTH;

	if (0u != (fsasthValue & LL_NVM_VALUE_U08_MASK1_FSASTH_SQEND))
	{
		LL_NVM_REGISTER_U08_FSSQ = LL_NVM_VALUE_U08_FSSQ_CLEAR;
	}
	else
	{
		status = false;
	}

	return status;
}

static bool isSecondPhaseCompleted_(void)
{
	bool status = true;
	const uint8_t fsasthValue = LL_NVM_REGISTER_U08_FSASTH;

	if (0u != (fsasthValue & LL_NVM_VALUE_U08_MASK1_FSASTH_SQEND))
	{
		status = false;
	}

	return status;
}

static void getSequencerStatus_(uint8_t* const status)
{
	DEBUG_ASSERT(status);
	const uint8_t fsastlValue = LL_NVM_REGISTER_U08_FSASTL;
	*status = fsastlValue & LL_NVM_VALUE_U08_MASK1_FSASTL_ERROR_FLAG;
}

static void clearSequencerRegister_(void)
{
	LL_NVM_REGISTER_U08_FLRST = LL_NVM_VALUE_U08_FLRST_ON;
	NOP();
	LL_NVM_REGISTER_U08_FLRST = LL_NVM_VALUE_U08_FLRST_OFF;
	NOP();
}
