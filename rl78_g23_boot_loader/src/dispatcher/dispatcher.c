
/*
 * dispatcher.c
 *
 * Created on: 22 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "dispatcher/dispatcher.h"

#include "debug/asserts.h"

#include "ll_serial/ll_serial.h"

#include "l2/l2_serial.h"
#include "l2/l2_network.h"

#include "l3/l3_packet.h"
#include "l3/l3_network.h"

#include "events/events.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//
#define DISPATCHER_TIMOUT_IN_MS 1000



////////////////////////////
// PRIVATE ATTRIBUTES
//
static L2_SerialNetwork_t bottomL2SerialNetwork_;
static L2_SerialNetwork_t topL2SerialNetwork_;

static bool busy_ = false;

// TODO: review this block:
// [
static bool timerInUse_ = false;
static LL_Time_t timer_ = 0;
// ]



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
static void handleOnSentPacket_(
	const L3_TxStatus_t success,
	L3_PacketHandle_t packet,
	void* arguments);



////////////////////////////
// IMPLEMENTATION
//
void Dispatcher_init(void)
{
	LL_Serial_open(LL_SERIAL_TYPE_UART1);
	LL_Serial_open(LL_SERIAL_TYPE_UART2);

	L2_SerialNetwork_create(&bottomL2SerialNetwork_, LL_SERIAL_TYPE_UART1);
	L2_SerialNetwork_create(&topL2SerialNetwork_, LL_SERIAL_TYPE_UART2);

	L3_Network_create(
		(L2_NetworkHandle_t)&bottomL2SerialNetwork_,
		(L2_NetworkHandle_t)&topL2SerialNetwork_
	);
}

void Dispatcher_listen(void)
{
	if (L3_Network_run())
	{
		L3_Packet_t l3Packet;

		if (L3_Network_receivePacket(&l3Packet))
		{
			Event_t event;
			event.type = EVENT_TYPE_DISPATCH_PACKET;
			event.l3Packet = l3Packet;
			(void)EventQueue_give(event);
		}
	}
}

void Dispatcher_enter(void)
{
}

void Dispatcher_run(EventHandle_t event)
{
	if (event->type != EVENT_TYPE_DISPATCH_PACKET)
	{
		return;
	}

// TODO: review this block:
// [
	if (timerInUse_ && LL_Time_getElapsedMs(timer_) > DISPATCHER_TIMOUT_IN_MS)
	{
		busy_ = false;
		timerInUse_ = false;
	}
// ]

	if (busy_)
	{
		return;
	}

	if (L3_Network_sendPacket(
			&event->l3Packet,
			L3_NETWORK_TX_CALLBACK(
				handleOnSentPacket_,
				NULL)))
	{
		busy_ = true;
		timerInUse_ = true;
		timer_ = LL_Time_getMs();
	}
	else
	{
		(void)EventQueue_give(*event);
		busy_ = false;
	}
}

void Dispatcher_exit(void)
{
}

static void handleOnSentPacket_(
	const L3_TxStatus_t success,
	L3_PacketHandle_t packet,
	void* arguments)
{
	(void)success;
	(void)packet;
	(void)arguments;

	busy_ = false;

	if (success == L3_TX_STATUS_SEND_FAILED)
	{
		Event_t event;
		event.type = EVENT_TYPE_DISPATCH_PACKET;
		event.l3Packet = *packet;
		(void)EventQueue_give(event);
	}
}
