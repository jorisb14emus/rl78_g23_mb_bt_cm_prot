
/*
 * ll_time.c
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "ll_time/ll_time.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// GLOBAL ATTRIBUTES
//
uint64_t ticksInMilliseconds_ = 0;



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void LL_Time_init(void)
{
	// NOTE: these externs are used to supress the intellisense  warning:
	extern void R_Config_TAU0_3_Create(void);
	extern void R_Config_TAU0_3_Lower8bits_Start(void);
	R_Config_TAU0_3_Create();
	R_Config_TAU0_3_Lower8bits_Start();
}

LL_Time_t LL_Time_getMs(void)
{
	LL_Time_t time;
	// Assuming this mask is never changed from within interrupts.
	// Else might need critical section or atomic access here
	uint8_t tmmk00 = TMMK00;
	TMMK00 = 1U;		/* mask INTTM10 interrupt */
	time = (uint32_t)ticksInMilliseconds_;
	TMMK00 = tmmk00;	/* restore INTTM10 interrupt mask */
	return time;
}

LL_Time_t LL_Time_getElapsedMs(const LL_Time_t start)
{
	return LL_Time_getMs() - start;
}

void LL_Time_delay(const LL_Time_t milliseconds)
{
	const LL_Time_t time0 = LL_Time_getMs();
	while (LL_Time_getElapsedMs(time0) < milliseconds);
}
