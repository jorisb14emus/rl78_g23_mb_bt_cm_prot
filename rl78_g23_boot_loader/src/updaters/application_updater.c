
/*
 * application_updater.c
 *
 * Created on: 15 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "updaters/application_updater.h"

#include "linker_config.h"

#include "debug/asserts.h"

#include "ll_nvm/ll_nvm.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"

#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// PRIVATE TYPES AND MACROS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static uint8_t blockIndex_ = APPLICATION_CFM_START_BLOCK_INDEX;
static bool finishedBlock_ = false;
static uint16_t blockCursor_ = 0;



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void ApplicationUpdater_enter(void)
{
}

void ApplicationUpdater_run(EventHandle_t event)
{
	DEBUG_ASSERT(event->type == EVENT_TYPE_RECEIVED_APPLICATION_UPDATE_BLOCK);
	DEBUG_ASSERT(LL_Nvm_isInitialized());

	DI();

	if (!LL_Nvm_setFlashMode(LL_NVM_FLASH_MODE_CODE_PROGRAMMING))
	{
		return;
	}

	if (finishedBlock_)
	{
		// Bounds check:
		if (blockIndex_ + 1 >= APPLICATION_CFM_BLOCKS_COUNT)
		{
			return;
		}

		++blockIndex_;

		LL_Nvm_Cf_eraseBlock(blockIndex_);
		if (!LL_Nvm_await()) { return; }

		LL_Nvm_Cf_blankCheck(blockIndex_);
		if (!LL_Nvm_await()) { return; }

		blockCursor_ = 0;
	}

	const uint16_t relativeAddress = blockIndex_ * RL78G23_CFM_BLOCK_CAPACITY + blockCursor_;
	const uint8_t* const payload = L3_Packet_referenceBody(&event->l3Packet);
	DEBUG_ASSERT(payload);

	// TODO: handle cases when incoming update block is of length not divisable by 4!
	for (; blockCursor_ < event->l3Packet.header.bodyLength; blockCursor_ += 4)
	{
		LL_Nvm_Cf_writeBytes(relativeAddress, payload + blockCursor_);
		if (!LL_Nvm_await()) { return; }
	}

	(void)LL_Nvm_setFlashMode(LL_NVM_FLASH_MODE_UNPROGRAMMABLE);
	EI();
}

void ApplicationUpdater_exit(void)
{
}
