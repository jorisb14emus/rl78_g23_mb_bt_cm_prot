
/*
 * boot_loader_updater.h
 *
 * Created on: 15 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __UPDATERS__BOOT_LOADER_UPDATER_H__
#define __UPDATERS__BOOT_LOADER_UPDATER_H__

//////////////////////////
// INCLUDES
//
#include "events/events.h"



////////////////////////////
// TYPES AND MACROS
//



//////////////////////////
// PUBLIC INTERFACE
//
void BootLoaderUpdater_enter(void);

void BootLoaderUpdater_run(EventHandle_t event);

void BootLoaderUpdater_exit(void);

#endif /* __UPDATERS__BOOT_LOADER_UPDATER_H__ */
