
/*
 * ll_serial.h
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_SERIAL__LL_SERIAL_H__
#define __LL_SERIAL__LL_SERIAL_H__

//////////////////////////
// INCLUDES
//
#include "ll_time/ll_time.h"

#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
enum
{
	LL_SERIAL_TYPE_UART1 = 0,
	LL_SERIAL_TYPE_UART2,
	LL_SERIAL_TYPES_COUNT
};

typedef uint8_t LL_SerialType_t;

enum
{	
	LL_SERIAL_ERROR_NONE = 0,
	LL_SERIAL_ERROR_OVERRUN,
	LL_SERIAL_ERROR_PARITY_ACK,
	LL_SERIAL_ERROR_STOP_BIT
};

typedef uint8_t LL_SerialError_t;

typedef struct
{
	void(*function)(void* arguments);
	void* arguments;
} LL_SerialTxCallback_t;

#define LL_SERIAL_TX_CALLBACK(_function, _arguments) \
	(LL_SerialTxCallback_t) { .function = _function, .arguments = _arguments }



//////////////////////////
// PUBLIC INTERFACE
//
void LL_Serial_open(const LL_SerialType_t serialType);

bool LL_Serial_sendBlocking(
	const LL_SerialType_t serialType,
	const uint8_t* const txBuffer,
	const uint16_t txLength,
	const LL_Time_t timeout);

bool LL_Serial_sendNonBlocking(
	const LL_SerialType_t serialType,
	const uint8_t* const txBuffer,
	const uint16_t txLength,
	const LL_SerialTxCallback_t txCallback);

bool LL_Serial_receiveByte(
	const LL_SerialType_t serialType,
	uint8_t* const byte,
	const LL_Time_t timeout);

// NOTE: These extern functions are for the use within the Renesas vendor
//       functions/drivers. Do not use them in your own code!
// [
void LL_Serial_onReceived_(
	const LL_SerialType_t serialType,
	const uint8_t byte);

void LL_Serial_onSent_(const LL_SerialType_t serialType);

void LL_Serial_onError_(
	const LL_SerialType_t serialType,
	const LL_SerialError_t errorType);
// ]

#endif /* __LL_SERIAL__LL_SERIAL_H__ */
