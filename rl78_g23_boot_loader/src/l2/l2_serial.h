
/*
 * l2_serial.h
 *
 * Created on: 11 Sep 2023
 *     Author: Paulius Tumelis (paulius.tumelis@emusbms.com)
 */

#ifndef __L2__L2_SERIAL_H__
#define __L2__L2_SERIAL_H__

//////////////////////////
// INCLUDES
//
#include "ll_serial/ll_serial.h"

#include "l2/l2_network.h"

#include <stdint.h>
#include <stddef.h>



////////////////////////////
// TYPES AND MACROS
//
#define container_of(ptr, type, member) ((type *)((char *)(1 ? (ptr) : &((type *)0)->member) - offsetof(type, member)))

typedef enum
{
	RX_STAGE_SYNC,
	RX_STAGE_LEN,
	RX_STAGE_L2ID,
	RX_STAGE_DATA,
	RX_STAGE_CRC_1,
	RX_STAGE_CRC_2,
	RX_STAGE_ACCEPT,
	RX_STAGE_FINISHED,
} L2_PacketRxStage_t;

typedef enum
{
	TX_STAGE_IDLE,
	TX_STAGE_HEAD,
	TX_STAGE_PAYLOAD,
	TX_STAGE_CRC,
	TX_STAGE_WAIT_ACK,
} L2_PacketTxStage_t;

typedef struct
{
	L2_PacketRxStage_t stage;
	uint8_t dataCnt;
	uint16_t crc16;
	LL_Time_t rxStartTimestamp;
	uint8_t seqLastAccepted;
} L2_SerialRxState_t;

typedef struct
{
	L2_PacketTxStage_t stage;
	L2_PacketDescriptorHeader_t header;
	const uint8_t * payload;
	uint16_t crc16;
	LL_Time_t txEndTimestamp;
	uint8_t retryCount;
	uint8_t seqLastReceived;
	volatile bool busy;
} L2_SerialTxState_t;

typedef struct
{
	L2_Network_t super;
	const LL_SerialType_t uartId;
	L2_Packet_t rxBuffer;
	struct
	{
		L2_PacketDescriptorHeader_t header;
		uint16_t crc16;
	} responseBuffer;
	L2_SerialRxState_t rxState;
	L2_SerialTxState_t txState;
} L2_SerialNetwork_t;



//////////////////////////
// PUBLIC INTERFACE
//
void L2_SerialNetwork_create(
	L2_SerialNetwork_t* handle,
	LL_SerialType_t uartId);

#endif // __L2__L2_SERIAL_H__
