
/*
 * l2_serial.c
 *
 * Created on: 11 Sep 2023
 *     Author: Paulius Tumelis (paulius.tumelis@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "l2/l2_serial.h"

#include "ll_leds/ll_leds.h"
#include "ll_crc/ll_crc.h"
#include "ll_time/ll_time.h"
#include "ll_serial/ll_serial.h"

#include "l2/l2_network.h"

#include <string.h>



////////////////////////////
// PRIVATE TYPES AND MACROS
//
#define CALLBACK(_func, ...) do { if (_func != NULL) {_func(__VA_ARGS__);} } while(0)

// Consumption bytes count define, to not create uneeded variable, since its done in compile time
const uint16_t INITIAL_CRC16_VALUE = 0x0000;
const uint8_t DATAID_OFFSET = 3;
const uint8_t DATAID_MASK = (uint8_t)(0xFF << DATAID_OFFSET);



//////////////////////////
//	PRIVATE ATTRIBUTES
//



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
static void sendFramePartCb_(L2_SerialNetwork_t* handle);

static void l2ResponseCrcCb_(L2_SerialNetwork_t* handle);

static void l2ResponseHeaderCb_(L2_SerialNetwork_t* handle);

static bool tryRetransmit_(L2_SerialNetwork_t* handle);

static bool trySendL2Response_(
	L2_SerialNetwork_t* handle,
	L2_PacketType_t type,
	uint8_t seqNumber);

static uint16_t calculateFrameCrc_(
	L2_PacketType_t dataId,
	uint8_t seqNumber,
	const uint8_t* payload,
	const uint8_t payload_len);



////////////////////////////
// IMPLEMENTATION
//
/**
 * @brief Public method for sending specific L2Type packet
 * @note Peer intended to respond ACK or NACK response packet based on availability
 * 
 * @param[in/out] h 			- L2SerialNetwork object handle
 * @param[in] packet			- packet to transmit
 * @param[in] cb				- sending completion callback
 */
static L2_SendingResult_t L2_SerialNetwork_send(
	L2_NetworkHandle_t h,
	const uint8_t payload[64],
	const uint8_t payload_len,
	L2_SendCompleteCb_f cb)
{
	L2_SerialNetwork_t* h_ = (L2_SerialNetwork_t*)h;

	// TODO: maybe infer tx availability status by checking for sendCompleteCallback NULL for equality 
	if(h_->txState.busy || h_->txState.stage != TX_STAGE_IDLE) return SEND_ERR_BUSY;
	h_->txState.busy = true;
	
	h_->txState.stage = TX_STAGE_HEAD;
	h_->txState.retryCount = SEND_RETRY_COUNT;
	
	h_->txState.header.syncByte = SYNC_BYTE;
	h_->txState.header.dataId = L2TYPE_RAW;
	h_->txState.header.dataLength = payload_len;
	h_->txState.header.seqNumber = h_->txState.seqLastReceived + 1;

	// this is calculated by including transport data length + dataTypeId and sequence length + crc16 length
	// packet->dataLength = packet->dataLength + sizeof(dataidAndSeqNumberByte) + sizeof(uint16_t);
	h_->txState.payload = payload;
	h_->txState.crc16 = calculateFrameCrc_(h_->txState.header.dataId, h_->txState.header.seqNumber, payload, payload_len);
	h_->super.sendCompleteCallback = cb;

	// Indicating the send start event with green led
	LL_Led_set(LL_LED_TYPE_GREEN, LL_LED_VALUE_HIGH);

	// initiating a frame transmission by sending the header and registering a callback that handles the rest
	LL_Serial_sendNonBlocking(
		h_->uartId,
		(const uint8_t*) &h_->txState.header,
		sizeof(h_->txState.header),
		LL_SERIAL_TX_CALLBACK((void(*)(void*))sendFramePartCb_, h)
	);
	
	return SEND_OK;
}

/**
 * @brief Chain callback that handles L2 frame transmission piece by piece over UART
 * @note This function is called in interrupt context (UART transmit ISR)
 * 
 * @param[in/out] handle 			- handle of a L2 layer which is transmitting the frame
 */
static void sendFramePartCb_(L2_SerialNetwork_t* handle)
{
	L2_SerialTxState_t* tx = &((L2_SerialNetwork_t*)handle)->txState;
	switch (tx->stage)
	{
		case TX_STAGE_HEAD:
			tx->stage = TX_STAGE_PAYLOAD;
			LL_Serial_sendNonBlocking(
				handle->uartId,
				tx->payload,
				tx->header.dataLength,
				LL_SERIAL_TX_CALLBACK((void(*)(void*))sendFramePartCb_, handle)
			);
			break;
		case TX_STAGE_PAYLOAD:
			tx->stage = TX_STAGE_CRC;
			LL_Serial_sendNonBlocking(
				handle->uartId,
				(uint8_t*)&tx->crc16,
				sizeof(tx->crc16),
				LL_SERIAL_TX_CALLBACK((void(*)(void*))sendFramePartCb_, handle)
			);
			break;
		case TX_STAGE_CRC:
			tx->txEndTimestamp = LL_Time_getMs();
			tx->stage = TX_STAGE_WAIT_ACK;
			tx->busy = false;
			// Indicating the send end event with green led
			LL_Led_set(LL_LED_TYPE_GREEN, LL_LED_VALUE_LOW);
			break;
		default:
			tx->stage = TX_STAGE_IDLE;
			tx->busy = false;
			// Indicating the send end event with green led
			LL_Led_set(LL_LED_TYPE_GREEN, LL_LED_VALUE_LOW);
			break;
	}
}

static L2_PacketRxStage_t l2ReceiveByte(
	L2_SerialRxState_t* rx,
	uint8_t rxByte,
	L2_PacketHandle_t rxBuffer)
{
	switch (rx->stage)
	{
		case RX_STAGE_SYNC:
			if (rxByte == SYNC_BYTE)
			{
				rx->rxStartTimestamp = LL_Time_getMs();
				rx->dataCnt = 0;
				rx->stage = RX_STAGE_LEN;
			}
			break;
		
		case RX_STAGE_LEN:
			if (rxByte <= MAX_PACKET_DATA_LENGTH)
			{
				rxBuffer->dataLength = rxByte;
				rx->stage = RX_STAGE_L2ID;
			}
			else
			{
				rx->stage = RX_STAGE_SYNC;
			}
			break;
		
		case RX_STAGE_L2ID:
			rxBuffer->dataId = rxByte >> DATAID_OFFSET;
			rxBuffer->seqNumber = rxByte & ~DATAID_MASK;
			if (rxBuffer->dataLength > 0)
			{
				rx->stage = RX_STAGE_DATA;
			}
			else
			{
				rx->stage = RX_STAGE_CRC_1;
			}
			break;
		
		case RX_STAGE_DATA:
			rxBuffer->data[rx->dataCnt] = rxByte;
			rx->dataCnt += 1;
			if (rx->dataCnt >= rxBuffer->dataLength) rx->stage = RX_STAGE_CRC_1;
			break;
		
		case RX_STAGE_CRC_1:
			((uint8_t *) &rx->crc16)[0] = rxByte;
			rx->stage = RX_STAGE_CRC_2;
			break;
		
		case RX_STAGE_CRC_2:
			((uint8_t *) &rx->crc16)[1] = rxByte;
			rx->stage = RX_STAGE_FINISHED;
			break;

		case RX_STAGE_FINISHED:
			break;
		
		default:
			rx->stage = RX_STAGE_SYNC;
			break;
	}
	return rx->stage;
};

/**
 * @brief Public method for running a L2 protocol parser and reacting to received frames.
 * 
 * @param[in] h	- Handle of an L2 serial object.
 */
static bool L2_SerialNetwork_run(L2_NetworkHandle_t h)
{
	const LL_Time_t RX_TIMEOUT_MS = 10;
	const LL_Time_t TX_ACK_TIMEOUT_MS = 50;
	L2_SerialNetwork_t* h_ = (L2_SerialNetwork_t*)h;
	L2_SerialRxState_t* rx = &h_->rxState;
	L2_SerialTxState_t* tx = &h_->txState;
	
	// try acknowledge successfully received frame
	if (rx->stage == RX_STAGE_ACCEPT)
	{
		if (trySendL2Response_(h_, L2TYPE_ACK, h_->rxBuffer.seqNumber))
		{
			if (rx->seqLastAccepted != h_->rxBuffer.seqNumber)
			{
				rx->seqLastAccepted = h_->rxBuffer.seqNumber;
				rx->stage = RX_STAGE_FINISHED;
			}
			else
			{
				rx->stage = RX_STAGE_SYNC;
			}
		}
	}
	// process ongoing package reception
	else if (rx->stage != RX_STAGE_FINISHED)
	{
		// Timeout frame reception if complete frame is not received within RX_TIMEOUT_MS
		if (LL_Time_getElapsedMs(rx->rxStartTimestamp) > RX_TIMEOUT_MS)
		{
			rx->stage = RX_STAGE_SYNC;
		}
		
		uint8_t rxByte;
		uint8_t rxCnt = 8;
		while (
			LL_Serial_receiveByte(h_->uartId, &rxByte, 0) &&
			h_->rxState.stage != RX_STAGE_FINISHED &&
			h_->rxState.stage != RX_STAGE_ACCEPT &&
			rxCnt > 0)
		{
			rxCnt -= 1;
			if (l2ReceiveByte(rx, rxByte, &h_->rxBuffer) != RX_STAGE_FINISHED) continue;
				
			uint16_t crc = calculateFrameCrc_(h_->rxBuffer.dataId, h_->rxBuffer.seqNumber, h_->rxBuffer.data, h_->rxBuffer.dataLength);

			if (rx->crc16 != crc)
			{
				rx->stage = RX_STAGE_SYNC;
				continue;
			}
			
			switch (h_->rxBuffer.dataId)
			{
				case L2TYPE_ACK:
				case L2TYPE_NACK:
					if (tx->stage == TX_STAGE_WAIT_ACK &&
						tx->seqLastReceived != h_->rxBuffer.seqNumber)
					{
						// not checking sequence numbering at the moment
						// only using seqNumber as 1bit counter
						tx->seqLastReceived = h_->rxBuffer.seqNumber;
						CALLBACK(
							h_->super.sendCompleteCallback,
							(L2_PacketHandle_t)(tx->payload),
							h_->rxBuffer.dataId == L2TYPE_ACK ? true : false);
						tx->stage = TX_STAGE_IDLE;
					}
					rx->stage = RX_STAGE_SYNC;
					break;

				case L2TYPE_PING:
					rx->stage = RX_STAGE_SYNC;
					break;

				case L2TYPE_RAW:
					if (trySendL2Response_(h_, L2TYPE_ACK, h_->rxBuffer.seqNumber))
					{
						if (rx->seqLastAccepted != h_->rxBuffer.seqNumber)
						{
							rx->seqLastAccepted = h_->rxBuffer.seqNumber;
							rx->stage = RX_STAGE_FINISHED;
						}
						else rx->stage = RX_STAGE_SYNC;
					}
					else rx->stage = RX_STAGE_ACCEPT;
					break;

				default:
					rx->stage = RX_STAGE_SYNC;
					break;
			}
		}
	}

	// Timeout frame transmission if it isn't acknowledged longer than TX_TIMEOUT_MS
	/*
	 * Note if predicate sequence is important here - need to check busy flag first,
	 * because it is modified in interrupt context, otherwise we might be in inconsistent state
	 */
	if (!tx->busy &&
		LL_Time_getElapsedMs(tx->txEndTimestamp) > TX_ACK_TIMEOUT_MS &&
		tx->stage == TX_STAGE_WAIT_ACK)
	{
		if (!tryRetransmit_(h_))
		{
			CALLBACK(
				h_->super.sendCompleteCallback,
				(L2_PacketHandle_t)(tx->payload),
				false);
			tx->stage = TX_STAGE_IDLE;
		}
	}

	return rx->stage == RX_STAGE_FINISHED ? true : false;
}

static bool tryRetransmit_(L2_SerialNetwork_t* handle)
{
	if (handle->txState.retryCount > 0)
	{
		handle->txState.retryCount -= 1;
		handle->txState.stage = TX_STAGE_HEAD;
		handle->txState.busy = true;
		LL_Serial_sendNonBlocking(
			handle->uartId,
			(const uint8_t*) &handle->txState.header,
			sizeof(handle->txState.header),
			LL_SERIAL_TX_CALLBACK((void(*)(void*))sendFramePartCb_, handle)
		);
		return true;
	}
	return false;
}

static L2_ReceptionResult_t L2_SerialNetwork_receive(
	L2_NetworkHandle_t h,
	L2_PacketHandle_t* packet)
{
	L2_SerialNetwork_t* h_ = (L2_SerialNetwork_t*)h;
	*packet = NULL;
	
	if (h_->rxState.stage == RX_STAGE_FINISHED)
	{
		h_->rxState.stage = RX_STAGE_SYNC;
		*packet = &h_->rxBuffer;
		return RECV_OK;
	}
	return RECV_ONGOING;
}

static void l2ResponseCrcCb_(L2_SerialNetwork_t* handle)
{
	handle->txState.busy = false;
}

static void l2ResponseHeaderCb_(L2_SerialNetwork_t* handle)
{
	LL_Serial_sendNonBlocking(
		handle->uartId,
		(uint8_t*)&handle->responseBuffer.crc16,
		sizeof(handle->responseBuffer.crc16),
		LL_SERIAL_TX_CALLBACK((void(*)(void*))l2ResponseCrcCb_, handle)
	);
}

static bool trySendL2Response_(
	L2_SerialNetwork_t* handle,
	L2_PacketType_t type,
	uint8_t seqNumber)
{
	L2_PacketDescriptorHeader_t* header = &handle->responseBuffer.header;
	if (handle->txState.busy) return false;
	handle->txState.busy = true;
	
	header->syncByte = SYNC_BYTE;
	header->dataId = type;
	header->seqNumber = seqNumber;
	header->dataLength = 0;
	handle->responseBuffer.crc16 = calculateFrameCrc_(header->dataId, header->seqNumber, NULL, header->dataLength);
	LL_Serial_sendNonBlocking(
		handle->uartId,
		(const uint8_t*) header,
		sizeof(*header),
		LL_SERIAL_TX_CALLBACK((void(*)(void*))l2ResponseHeaderCb_, handle)
	);
	return true;
}

static uint16_t calculateFrameCrc_(
	L2_PacketType_t dataId,
	uint8_t seqNumber,
	const uint8_t* payload,
	const uint8_t payload_len)
{
	uint16_t calculatedCrc16 = 0;
	uint8_t dataIdAndSeq = (uint8_t)(dataId << DATAID_OFFSET) | (seqNumber & ~DATAID_MASK);
	// TODO: maybe calculate CRC over length aswel
	calculatedCrc16 = LL_Crc_calc16(&dataIdAndSeq, sizeof(dataIdAndSeq), INITIAL_CRC16_VALUE);
	calculatedCrc16 = LL_Crc_calc16(payload, payload_len, calculatedCrc16);
	return calculatedCrc16;
}

void L2_SerialNetwork_create(
	L2_SerialNetwork_t* handle,
	LL_SerialType_t uartId)
{
	handle->super.send = L2_SerialNetwork_send;
	handle->super.receive = L2_SerialNetwork_receive;
	handle->super.run = L2_SerialNetwork_run;

	*(LL_SerialType_t*)&handle->uartId = uartId; // assigning a const field in constructor

	handle->txState.stage = TX_STAGE_IDLE;

	handle->rxState.stage = RX_STAGE_SYNC;
	handle->rxState.seqLastAccepted = 0xff; // init to invalid value to initialy accept any seqNum
}
