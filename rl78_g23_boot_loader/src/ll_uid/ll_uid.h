
/*
 * ll_uid.h
 *
 * Created on: 06 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_UID__LL_UID_H__
#define __LL_UID__LL_UID_H__

//////////////////////////
// INCLUDES
//
#include "linker_config.h"

#include <stdbool.h>
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
typedef struct
{
	uint8_t id[RL78G23_UNIQUE_ID_LENGTH];
} LL_UniqueId_t;

typedef struct
{
	uint8_t name[RL78G23_PRODUCT_LENGTH];
} LL_ProductName_t;



////////////////////////////
// PUBLIC INTERFACE
//
void LL_Uid_readUniqueId(LL_UniqueId_t* const uniqueId);

void LL_Uid_readProductName(LL_ProductName_t* const productName);

#endif /* __LL_UID__LL_UID_H__ */
