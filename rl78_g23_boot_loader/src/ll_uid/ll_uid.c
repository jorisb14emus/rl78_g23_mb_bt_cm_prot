
/*
 * ll_uid.c
 *
 * Created on: 06 Oct 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "ll_uid/ll_uid.h"

#include "debug/asserts.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void LL_Uid_readUniqueId(LL_UniqueId_t* const uniqueId)
{
	DEBUG_ASSERT(uniqueId);
	uint8_t __far* value = (uint8_t __far*)RL78G23_UNIQUE_ID_START_ADDRESS;

	for (uint8_t index = 0; index < RL78G23_UNIQUE_ID_LENGTH; ++index, ++value)
	{
		uniqueId->id[index] = *value;
	}
}

void LL_Uid_readProductName(LL_ProductName_t* const productName)
{
	DEBUG_ASSERT(productName);
	uint8_t __far* value = (uint8_t __far*)RL78G23_PRODUCT_START_ADDRESS;

	for (uint8_t index = 0; index < RL78G23_PRODUCT_LENGTH; ++index, ++value)
	{
		productName->name[index] = *value;
	}
}
