
/*
 * rl78_g23_master_boot.c
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "linker_config.h"

#include "ll_dfl/ll_dfl.h"
#include "ll_crc/ll_crc.h"

#include "r_smc_entry.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static MasterBootConfig_t  masterBootConfig  = {0};



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
signed int Main_masterBootInvoke(void);

static void jumpToAddress(const uint32_t address);




////////////////////////////
// IMPLEMENTATION
//
signed int Main_masterBootInvoke(void)
{
	EI();

	// TODO: Should master boot try to check/validate the boot loader's and application's cfg sections?

	LL_Dfl_open(LL_DFL_FREQUENCY_32MHz);
	LL_Dfl_read(MASTER_BOOT_CFG_START_ADDRESS, (uint8_t* const)&masterBootConfig,  MASTER_BOOT_CFG_LENGTH);
	LL_Dfl_close();

	LL_Crc_init();
	const LL_Crc16_t masterBootConfigCrc = LL_Crc_calc16(
		(const uint8_t* const)&masterBootConfig,
		sizeof(MasterBootConfig_t) - sizeof(LL_Crc16_t),
		0
	);

	if (masterBootConfigCrc != masterBootConfig.crc)
	{
		// Invalid 'masterBootConfig'!
		goto error;
	}

	// Jumping to either boot loader or application depending on the boot priority flag
	if (BOOT_LOADER_VALUE_PRIORITY == masterBootConfig.bootPriority)
	{
		jumpToAddress(BOOT_LOADER_CFM_START_ADDRESS);
	}
	else if (APPLICATION_VALUE_PRIORITY == masterBootConfig.bootPriority)
	{
		const uint32_t address = APPLICATION_CFM_START_ADDRESS;
		jumpToAddress(address);
	}

error:
	while (1) { NOP(); }
	return 0;
}

static void jumpToAddress(const uint32_t address)
{
	(void)address;
	__asm volatile (
		"br ax\n"
		"nop\n"
	);
}
