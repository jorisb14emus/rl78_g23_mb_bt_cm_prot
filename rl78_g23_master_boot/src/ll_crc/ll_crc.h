
/*
 * ll_crc.h
 *
 * Created on: 5 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_CRC__LL_CRC_H__
#define __LL_CRC__LL_CRC_H__

//////////////////////////
// INCLUDES
//
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
typedef uint16_t LL_Crc16_t;
#define LL_CRC16_SIZE sizeof(LL_Crc16_t)



//////////////////////////
// PUBLIC INTERFACE
//
void LL_Crc_init(void);

LL_Crc16_t LL_Crc_calc16(
	const uint8_t* const data,
	const uint16_t length,
	const LL_Crc16_t initial);

#endif /* __LL_CRC__LL_CRC_H__ */
