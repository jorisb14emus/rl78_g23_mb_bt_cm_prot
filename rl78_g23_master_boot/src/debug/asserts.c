
/*
 * asserts.c
 *
 * Created on: 11 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

//////////////////////////
// INCLUDES
//
#include "debug/asserts.h"



////////////////////////////
// PRIVATE TYPES AND MACROS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// GLOBAL ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
#ifdef NDEBUG
#else
void Debug_assert(const bool expression)
{
	if (!expression)
	{
		extern void PowerON_Reset(void);
		PowerON_Reset();
	}
}
#endif
