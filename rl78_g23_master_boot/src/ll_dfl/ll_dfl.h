
/*
 * ll_dfl.h
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_DFL__LL_DFL_H__
#define __LL_DFL__LL_DFL_H__

//////////////////////////
// INCLUDES
//
#include <stdint.h>



////////////////////////////
// TYPES AND MACROS
//
enum
{
	LL_DFL_FREQUENCY_1MHz = 0x00u,
	LL_DFL_FREQUENCY_2MHz = 0x01u,
	LL_DFL_FREQUENCY_3MHz = 0x02u,
	LL_DFL_FREQUENCY_4MHz = 0x03u,
	LL_DFL_FREQUENCY_5MHz = 0x04u,
	LL_DFL_FREQUENCY_6MHz = 0x05u,
	LL_DFL_FREQUENCY_7MHz = 0x06u,
	LL_DFL_FREQUENCY_8MHz = 0x07u,
	LL_DFL_FREQUENCY_9MHz = 0x08u,
	LL_DFL_FREQUENCY_10MHz = 0x09u,
	LL_DFL_FREQUENCY_11MHz = 0x0Au,
	LL_DFL_FREQUENCY_12MHz = 0x0Bu,
	LL_DFL_FREQUENCY_13MHz = 0x0Cu,
	LL_DFL_FREQUENCY_14MHz = 0x0Du,
	LL_DFL_FREQUENCY_15MHz = 0x0Eu,
	LL_DFL_FREQUENCY_16MHz = 0x0Fu,
	LL_DFL_FREQUENCY_17MHz = 0x10u,
	LL_DFL_FREQUENCY_18MHz = 0x11u,
	LL_DFL_FREQUENCY_19MHz = 0x12u,
	LL_DFL_FREQUENCY_20MHz = 0x13u,
	LL_DFL_FREQUENCY_21MHz = 0x14u,
	LL_DFL_FREQUENCY_22MHz = 0x15u,
	LL_DFL_FREQUENCY_23MHz = 0x16u,
	LL_DFL_FREQUENCY_24MHz = 0x17u,
	LL_DFL_FREQUENCY_25MHz = 0x18u,
	LL_DFL_FREQUENCY_26MHz = 0x19u,
	LL_DFL_FREQUENCY_27MHz = 0x1Au,
	LL_DFL_FREQUENCY_28MHz = 0x1Bu,
	LL_DFL_FREQUENCY_29MHz = 0x1Cu,
	LL_DFL_FREQUENCY_30MHz = 0x1Du,
	LL_DFL_FREQUENCY_31MHz = 0x1Eu,
	LL_DFL_FREQUENCY_32MHz = 0x1Fu,
	LL_DFL_FREQUENCIES_COUNT
};

typedef uint8_t LL_DflFrequency_t;



//////////////////////////
// PUBLIC INTERFACE
//
void LL_Dfl_open(const LL_DflFrequency_t frequency);

void LL_Dfl_close(void);

void LL_Dfl_read(
	const uint32_t address20,
	uint8_t* const buffer,
	const uint16_t length);

#endif /* __LL_DFL__LL_DFL_H__ */
